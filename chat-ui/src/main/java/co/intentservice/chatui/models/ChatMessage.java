package co.intentservice.chatui.models;

import android.text.format.DateFormat;

import java.util.concurrent.TimeUnit;


/**
 *
 * Chat Message model used when ChatMessages are required, either to be sent or received,
 * all messages that are to be shown in the chat-ui must be contained in this model.
 *
 */
public class ChatMessage {
    private String message;
    private long timestamp;
    private String filePath;
    private int readStatus;
    private int type;

    public ChatMessage(String message, long timestamp, int type){
        this.message = message;
        this.timestamp = timestamp;
        this.type = type;
    }

    public ChatMessage(String message, long timestamp, String filePath, int readStatus, int type) {
        this.message = message;
        this.timestamp = timestamp;
        this.filePath = filePath;
        this.readStatus = readStatus;
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public String getFormattedTime(){

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;

        long timeDifference = System.currentTimeMillis() - timestamp;

        return timeDifference < oneDayInMillis
                ? DateFormat.format("hh:mm a", timestamp).toString()
                : DateFormat.format("dd MMM - hh:mm a", timestamp).toString();
    }

    public interface Type{
        public static final int TYPE_VIEW_REC_MSG_TEXT_0 = 0;
        public static final int TYPE_VIEW_SEN_MSG_TEXT_1 = 1;
        public static final int TYPE_VIEW_REC_MSG_IMG_2 = 2;
        public static final int TYPE_VIEW_SEN_MSG_IMG_3 = 3;
        public static final int TYPE_VIEW_REC_MSG_VIDEO_4 = 4;
        public static final int TYPE_VIEW_SEN_MSG_VIDEO_5 = 5;
        public static final int TYPE_VIEW_REC_MSG_AUDIO_6 = 6;
        public static final int TYPE_VIEW_SEN_MSG_AUDIO_7 = 7;
        public static final int TYPE_VIEW_REC_MSG_DOC_8 = 8;
        public static final int TYPE_VIEW_SEN_MSG_DOC_9 = 9;
    }
}
