package co.intentservice.chatui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import co.intentservice.chatui.R;

public class PDFActivity extends AppCompatActivity {
   private  String docPath="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (getIntent() != null) {
            if (getIntent().getStringExtra("DocPath") != null && getIntent().getStringExtra("DocPath") != null)
                docPath = intent.getStringExtra("DocPath");
        }
        WebView webView=new WebView(PDFActivity.this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new Callback());
        /*webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + imagePath);*/
        String pdfURL = "http://dl.dropboxusercontent.com/u/37098169/Course%20Brochures/AND101.pdf";
        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + docPath);
        setContentView(webView);
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }
    }

