package co.intentservice.chatui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import co.intentservice.chatui.lemda.Fun1ParamRet;
import co.intentservice.chatui.models.ChatMessage;

/**
 * Created by abul on 8/1/18.
 */

public abstract class GenericMultiViewAdapter<T0 extends ViewDataBinding, T1 extends ViewDataBinding,
        T2 extends ViewDataBinding,
        T3 extends ViewDataBinding,
        T4 extends ViewDataBinding,
        T5 extends ViewDataBinding,
        T6 extends ViewDataBinding,
        T7 extends ViewDataBinding,
        T8 extends ViewDataBinding,
        T9 extends ViewDataBinding,
        L0> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private List<L0> mList0;
    private int mLayout0, mLayout1, mLayout2, mLayout3, mLayout4, mLayout5,
            mLayout6, mLayout7, mLayout8, mLayout9;
    private Fun1ParamRet<Integer, Integer> mFun;
    LayoutInflater inflater;


    public GenericMultiViewAdapter(Context context,
                                   int layout0, int layout1, int layout2, int layout3, int layout4, int layout5,
                                   int layout6, int layout7, int layout8, int layout9,
                                   Fun1ParamRet<Integer, Integer> fun) {
        mContext = context;
        mFun = fun;

        mList0 = new ArrayList<>();

        mLayout0 = layout0;
        mLayout1 = layout1;
        mLayout2 = layout2;
        mLayout3 = layout3;
        mLayout4 = layout4;
        mLayout5 = layout5;
        mLayout6 = layout6;
        mLayout7 = layout7;
        mLayout8 = layout8;
        mLayout9 = layout9;


        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (viewType) {
            case ChatMessage.Type.TYPE_VIEW_REC_MSG_TEXT_0:
                T0 binding0 = DataBindingUtil.inflate(inflater, mLayout0/*mFun.done()*/, parent, false);
                binding0.getRoot().setTag(viewType);
                return new MyHolder0(binding0.getRoot(), binding0);
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1:
                T1 binding1 = DataBindingUtil.inflate(inflater, mLayout1/*mFun.done()*/, parent, false);
                binding1.getRoot().setTag(viewType);
                return new MyHolder1(binding1.getRoot(), binding1);
            case ChatMessage.Type.TYPE_VIEW_REC_MSG_IMG_2:
                T2 binding2 = DataBindingUtil.inflate(inflater, mLayout2/*mFun.done()*/, parent, false);
                binding2.getRoot().setTag(viewType);
                return new MyHolder2(binding2.getRoot(), binding2);
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3:
                T3 binding3 = DataBindingUtil.inflate(inflater, mLayout3/*mFun.done()*/, parent, false);
                binding3.getRoot().setTag(viewType);
                return new MyHolder3(binding3.getRoot(), binding3);
            case ChatMessage.Type.TYPE_VIEW_REC_MSG_VIDEO_4:
                T4 binding4 = DataBindingUtil.inflate(inflater, mLayout4/*mFun.done()*/, parent, false);
                binding4.getRoot().setTag(viewType);
                return new MyHolder4(binding4.getRoot(), binding4);
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_VIDEO_5:
                T5 binding5 = DataBindingUtil.inflate(inflater, mLayout5/*mFun.done()*/, parent, false);
                binding5.getRoot().setTag(viewType);
                return new MyHolder5(binding5.getRoot(), binding5);
            case ChatMessage.Type.TYPE_VIEW_REC_MSG_AUDIO_6:
                T6 binding6 = DataBindingUtil.inflate(inflater, mLayout6/*mFun.done()*/, parent, false);
                binding6.getRoot().setTag(viewType);
                return new MyHolder6(binding6.getRoot(), binding6);
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7:
                T7 binding7 = DataBindingUtil.inflate(inflater, mLayout7/*mFun.done()*/, parent, false);
                binding7.getRoot().setTag(viewType);
                return new MyHolder7(binding7.getRoot(), binding7);
            case ChatMessage.Type.TYPE_VIEW_REC_MSG_DOC_8:
                T8 binding8 = DataBindingUtil.inflate(inflater, mLayout8/*mFun.done()*/, parent, false);
                binding8.getRoot().setTag(viewType);
                return new MyHolder8(binding8.getRoot(), binding8);
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_DOC_9:
                T9 binding9 = DataBindingUtil.inflate(inflater, mLayout9/*mFun.done()*/, parent, false);
                binding9.getRoot().setTag(viewType);
                return new MyHolder9(binding9.getRoot(), binding9);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyHolder0) {
            customFun0((T0) ((MyHolder0) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder1) {
            customFun1((T1) ((MyHolder1) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder2) {
            customFun2((T2) ((MyHolder2) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder3) {
            customFun3((T3) ((MyHolder3) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder4) {
            customFun4((T4) ((MyHolder4) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder5) {
            customFun5((T5) ((MyHolder5) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder6) {
            customFun6((T6) ((MyHolder6) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder7) {
            customFun7((T7) ((MyHolder7) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder8) {
            customFun8((T8) ((MyHolder8) holder).binding, position, mList0);
        } else if (holder instanceof MyHolder9) {
            customFun9((T9) ((MyHolder9) holder).binding, position, mList0);
        }
    }


    public abstract void customFun0(T0 binding, int position, List<L0> mList);

    public abstract void customFun1(T1 binding, int position, List<L0> mList);

    public abstract void customFun2(T2 binding, int position, List<L0> mList);

    public abstract void customFun3(T3 binding, int position, List<L0> mList);

    public abstract void customFun4(T4 binding, int position, List<L0> mList);

    public abstract void customFun5(T5 binding, int position, List<L0> mList);

    public abstract void customFun6(T6 binding, int position, List<L0> mList);

    public abstract void customFun7(T7 binding, int position, List<L0> mList);

    public abstract void customFun8(T8 binding, int position, List<L0> mList);

    public abstract void customFun9(T9 binding, int position, List<L0> mList);


    @Override
    public int getItemCount() {
        return  mList0.size();
    }


    @Override
    public int getItemViewType(int position) {
        return mFun.done(position);
    }


    public class MyHolder0<T0> extends RecyclerView.ViewHolder {
        public T0 binding;

        public MyHolder0(View itemView, T0 binding) {
            super(itemView);
            this.binding = binding;
        }
    }

    public class MyHolder1<T1> extends RecyclerView.ViewHolder {
        public T1 binding;

        public MyHolder1(View itemView, T1 binding) {
            super(itemView);
            this.binding = binding;
        }
    }

    public class MyHolder2<T2> extends RecyclerView.ViewHolder {
        public T2 binding;

        public MyHolder2(View itemView, T2 binding) {
            super(itemView);
            this.binding = binding;
        }
    }

    public class MyHolder3<T3> extends RecyclerView.ViewHolder {
        public T3 binding;

        public MyHolder3(View itemView, T3 binding) {
            super(itemView);
            this.binding = binding;
        }
    }

    public class MyHolder4<T4> extends RecyclerView.ViewHolder {
        public T4 binding;

        public MyHolder4(View itemView, T4 binding) {
            super(itemView);
            this.binding = binding;
        }
    }

    public class MyHolder5<T5> extends RecyclerView.ViewHolder {
        public T5 binding;

        public MyHolder5(View itemView, T5 binding) {
            super(itemView);
            this.binding = binding;
        }
    }


    public class MyHolder6<T6> extends RecyclerView.ViewHolder {
        public T6 binding;

        public MyHolder6(View itemView, T6 binding) {
            super(itemView);
            this.binding = binding;
        }
    }


    public class MyHolder7<T7> extends RecyclerView.ViewHolder {
        public T7 binding;

        public MyHolder7(View itemView, T7 binding) {
            super(itemView);
            this.binding = binding;
        }
    }


    public class MyHolder8<T8> extends RecyclerView.ViewHolder {
        public T8 binding;

        public MyHolder8(View itemView, T8 binding) {
            super(itemView);
            this.binding = binding;
        }
    }


    public class MyHolder9<T9> extends RecyclerView.ViewHolder {
        public T9 binding;

        public MyHolder9(View itemView, T9 binding) {
            super(itemView);
            this.binding = binding;
        }
    }


    /*custom*/
    public List<L0> getList(){
        return mList0;
    }

    public void addMessage(L0 message) {
        mList0.add(message);
        notifyDataSetChanged();
    }

    public void addMessages(List<L0> chatMessages) {
        this.mList0.addAll(chatMessages);
        notifyDataSetChanged();
    }

    public void removeMessage(int position) {
        if (this.mList0.size() > position) {
            this.mList0.remove(position);
        }
    }

    public void clearMessages() {
        this.mList0.clear();
        notifyDataSetChanged();
    }


}