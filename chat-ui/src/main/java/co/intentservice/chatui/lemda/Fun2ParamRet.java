package co.intentservice.chatui.lemda;

/**
 * Created by abul on 12/12/17.
 */

@FunctionalInterface
public interface Fun2ParamRet<T,E,R> extends Fun{
    R done(T t, E e);
}
