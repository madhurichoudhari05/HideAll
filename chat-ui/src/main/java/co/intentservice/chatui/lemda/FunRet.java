package co.intentservice.chatui.lemda;

/**
 * Created by abul on 12/12/17.
 */

@FunctionalInterface
public interface FunRet<T> extends Fun{
    T done();
}
