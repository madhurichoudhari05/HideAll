package co.intentservice.chatui.lemda;

/**
 * Created by abul on 12/12/17.
 */
@FunctionalInterface
public interface Fun1ParamRet<T,R> extends Fun{
    R done(T t);
}
