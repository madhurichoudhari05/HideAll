package co.intentservice.chatui.lemda;

/**
 * Created by abul on 12/12/17.
 */

@FunctionalInterface
public interface Fun2Param<T,R> extends Fun{
    void done(T t, R r);
}
