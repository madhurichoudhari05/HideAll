package co.intentservice.chatui.lemda;

/**
 * Created by abul on 12/12/17.
 */

@FunctionalInterface
public interface Fun3Param<T,E,R> extends Fun{
    void done(T t, E e, R r);
}
