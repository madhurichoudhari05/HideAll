package co.intentservice.chatui;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import co.intentservice.chatui.activity.AppConstants;
import co.intentservice.chatui.activity.CommonUtils;
import co.intentservice.chatui.activity.PDFActivity;
import co.intentservice.chatui.activity.VideoActivity;
import co.intentservice.chatui.adapters.GenericMultiViewAdapter;
import co.intentservice.chatui.databinding.ChatAudioItemRcvBinding;
import co.intentservice.chatui.databinding.ChatAudioItemSentBinding;
import co.intentservice.chatui.databinding.ChatDocItemRcvBinding;
import co.intentservice.chatui.databinding.ChatDocItemSentBinding;
import co.intentservice.chatui.databinding.ChatImageItemRcvBinding;
import co.intentservice.chatui.databinding.ChatImageItemSentBinding;
import co.intentservice.chatui.databinding.ChatTextItemRcvBinding;
import co.intentservice.chatui.databinding.ChatTextItemSentBinding;
import co.intentservice.chatui.databinding.ChatVideoItemRcvBinding;
import co.intentservice.chatui.databinding.ChatVideoItemSentBinding;
import co.intentservice.chatui.databinding.ChatViewBinding;

import co.intentservice.chatui.fab.FloatingActionsMenu;
import co.intentservice.chatui.lemda.Fun1ParamRet;
import co.intentservice.chatui.models.ChatMessage;
import madhu.MediaPlayerHandler;


/**
 * Created by timi on 17/11/2015.
 */
public class ChatView extends RelativeLayout {

    private static final int FLAT = 0;
    private static final int ELEVATED = 1;

    private ChatViewBinding binding;
    private CardView inputFrame;
    private RecyclerView chatListView;
    private EditText inputEditText;
    private AppCompatImageView ivCamera, ivAttachment,ivRecordVoice;
    private boolean statusAudiio;


    private FloatingActionsMenu actionsMenu;
    private RelativeLayout inputLayout;
    private boolean previousFocusState = false, useEditorAction, isTyping;

    public ChatViewBinding getBinding() {
        return binding;
    }

    private Runnable typingTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (isTyping) {
                isTyping = false;
                if (typingListener != null) typingListener.userStoppedTyping();
            }
        }
    };
    private TypingListener typingListener;
    private OnSentMessageListener onSentMessageListener;
    private OnAttachmentClickListener attachmentListener;
    private OnEmojiClickListener emojiClickListener;
    private GenericMultiViewAdapter<
            ChatTextItemRcvBinding,ChatTextItemSentBinding,ChatImageItemRcvBinding,ChatImageItemSentBinding,
            ChatVideoItemRcvBinding,ChatVideoItemSentBinding,ChatAudioItemRcvBinding,ChatAudioItemSentBinding,
            ChatDocItemRcvBinding,ChatDocItemSentBinding,
            ChatMessage> chatViewListAdapter;

    private int inputFrameBackgroundColor, backgroundColor;
    private int inputTextSize, inputTextColor, inputHintColor;
    private int sendButtonBackgroundTint, sendButtonIconTint;

    private float bubbleElevation;

    private int bubbleBackgroundRcv, bubbleBackgroundSend; // Drawables cause cardRadius issues. Better to use background color
    private Drawable sendButtonIcon, buttonDrawable;
    private TypedArray attributes, textAppearanceAttributes;
    private Context context;
    private ImageView emoji_btn,ivUserPic;


    ChatView(Context context) {
        this(context, null);
    }

    public ChatView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatView(Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);

    }


    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        binding= DataBindingUtil.inflate(LayoutInflater.from(getContext()),R.layout.chat_view,this,true);
//        LayoutInflater.from(getContext()).inflate(R.layout.chat_view, this, true);
        this.context = context;
        initializeViews();
        getXMLAttributes(attrs, defStyleAttr);
        setViewAttributes();
        setListAdapter();
        setButtonClickListeners();
        setUserTypingListener();
        setUserStoppedTypingListener();
        setUserClickListener();
    }

    private void initializeViews() {
        chatListView = (RecyclerView) findViewById(R.id.chat_list);
        inputFrame = (CardView) findViewById(R.id.input_frame);
        inputEditText = (EditText) findViewById(R.id.input_edit_text);
        actionsMenu = (FloatingActionsMenu) findViewById(R.id.sendButton);
        ivAttachment = (AppCompatImageView) findViewById(R.id.ivAttach);
        ivRecordVoice = (AppCompatImageView) findViewById(R.id.ivRecordVoice);
        ivCamera = (AppCompatImageView) findViewById(R.id.ivCam);
        emoji_btn = (ImageView) findViewById(R.id.emoji_btn);
        ivUserPic = (ImageView) findViewById(R.id.ivUserPic);
        inputLayout = findViewById(R.id.rlInput);
        final View rootView = findViewById(R.id.rootView);


    }

    private void getXMLAttributes(AttributeSet attrs, int defStyleAttr) {
        attributes = context.obtainStyledAttributes(attrs, R.styleable.ChatView, defStyleAttr, R.style.ChatViewDefault);
        getChatViewBackgroundColor();
        getAttributesForBubbles();
        getAttributesForInputFrame();
        getAttributesForInputText();
        getAttributesForSendButton();
        getUseEditorAction();
        attributes.recycle();
    }

    Fun1ParamRet<Integer,Integer> fun=(pos)->{
        return chatViewListAdapter.getList().get(pos).getType();
    };

    private void setListAdapter() {

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setStackFromEnd(true);
        chatListView.setLayoutManager(manager);


        chatViewListAdapter = new GenericMultiViewAdapter<
                ChatTextItemRcvBinding, ChatTextItemSentBinding, ChatImageItemRcvBinding, ChatImageItemSentBinding,
                ChatVideoItemRcvBinding, ChatVideoItemSentBinding, ChatAudioItemRcvBinding, ChatAudioItemSentBinding,
                ChatDocItemRcvBinding, ChatDocItemSentBinding,
                ChatMessage>(
                context,
                R.layout.chat_text_item_rcv,
                R.layout.chat_text_item_sent,
                R.layout.chat_image_item_rcv,
                R.layout.chat_image_item_sent,
                R.layout.chat_video_item_rcv,
                R.layout.chat_video_item_sent,
                R.layout.chat_audio_item_rcv,
                R.layout.chat_audio_item_sent,
                R.layout.chat_doc_item_rcv,
                R.layout.chat_doc_item_sent, fun) {
            @Override
            public void customFun0(ChatTextItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                String profilepic= CommonUtils.getPreferencesString(context, AppConstants.RECEIVER__PIC);
                Picasso.with(context)
                        .load("file://" +profilepic )
                        .placeholder(R.drawable.placeholder_user)
                        .error(R.drawable.placeholder_user)
                        .into(  binding.ivUserPic);
            }

            @Override
            public void customFun1(ChatTextItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                String profilepic= CommonUtils.getPreferencesString(context, AppConstants.SENDER_PROFILE_PIC);
                Picasso.with(context)
                        .load("file://" +profilepic )
                        .placeholder(R.drawable.placeholder_user)
                        .error(R.drawable.placeholder_user)
                        .into(  binding.ivUserPic);
            }

            @Override
            public void customFun2(ChatImageItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                Picasso.with(context).load(mList.get(position).getFilePath()).into(binding.ivChat);
            }

            @Override
            public void customFun3(ChatImageItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                Picasso.with(context).load(mList.get(position).getFilePath()).into(binding.ivChat);
            }

            @Override
            public void customFun4(ChatVideoItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                binding.play.setOnClickListener(v->{
                    Intent intent = new Intent(context, VideoActivity.class);
                    intent.putExtra("VideoPath",mList.get(position).getFilePath());
                  //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });

                // binding. videoView.setVideoPath("http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4");
               //  binding. videoView.setVideoPath(mList.get(position).getFilePath());
              //  binding. videoView.start();
               // Picasso.with(context).load(mList.get(position).getFilePath()).into(binding.VideoView);
            }

            @Override
            public void customFun5(ChatVideoItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                binding.play.setOnClickListener(v->{
                    Intent intent = new Intent(context, VideoActivity.class);
                    intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });
            }

            @Override
            public void customFun6(ChatAudioItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));


                MediaPlayerHandler playerHandler= new MediaPlayerHandler();
                binding.poetDetailPlayBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        statusAudiio=true;
                        if(statusAudiio){
                            playerHandler.startPlaying();;
                            statusAudiio=false;
                        }
                        else {
                            playerHandler.pausePlaying();
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.blackpause);

                        }
                    }
                });




                playerHandler.setSeekLisner(mList.get(position)
                        .getFilePath(), position, new MediaPlayerHandler.SeekListner() {
                    @Override
                    public void setSeekBarMaxLength(int setLength) {
                        binding.progessSeekBar.setMax(setLength);
                    }

                    @Override
                    public void setSeekBarCurrentProgress(int length) {
                        binding.progessSeekBar.setProgress(length);
                        binding.remiainTime.setText(length);



                    }

                    @Override
                    public void playStatus(int isPlaying) {
                        //binding.progessSeekBar.setSta(isPlaying);


                    }

                    @Override
                    public void invalidatePrevious() {

                    }

                    @Override
                    public void setTotalLength(int duration) {

                        binding.totalTime.setText(""+duration);

                    }
                });
            }

            @Override
            public void customFun7(ChatAudioItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));



               // MediaPlayerHandler playerHandler=MediaPlayerHandler.getInstance();
                MediaPlayerHandler playerHandler= new MediaPlayerHandler();
                binding.poetDetailPlayBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        statusAudiio=true;
                        if(statusAudiio){
                            playerHandler.startPlaying();;
                            statusAudiio=false;
                        }
                        else {
                            playerHandler.pausePlaying();

                            binding.poetDetailPlayBtn.setImageResource(R.drawable.pause);

                        }
                    }
                });


                binding.poetDetailPlayBtn.setOnClickListener(v->{
                    playerHandler.startPlaying();

                });
                playerHandler.setSeekLisner(mList.get(position).getFilePath(), position, new MediaPlayerHandler.SeekListner() {
                    @Override
                    public void setSeekBarMaxLength(int setLength) {
                        binding.progessSeekBar.setMax(setLength);
                    }

                    @Override
                    public void setSeekBarCurrentProgress(int length) {
                        binding.progessSeekBar.setProgress(length);
                        binding.remiainTime.setText(length);


                    }

                    @Override
                    public void playStatus(int isPlaying) {
                        //binding.progessSeekBar.setSta(isPlaying);


                    }

                    @Override
                    public void invalidatePrevious() {

                    }

                    @Override
                    public void setTotalLength(int duration) {
                        binding.totalTime.setText(""+duration);



                    }
                });
            }

            @Override
            public void customFun8(ChatDocItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                String path=mList.get(position).getFilePath();//it contain your path of image..im using a temp string..
                String filename=path.substring(path.lastIndexOf("/")+1);
                binding.tvFileNmae.setText(filename);
                binding.llPdf.setOnClickListener(v->{
                    Intent intent = new Intent(context, PDFActivity.class);
                    intent.putExtra("DocPath",mList.get(position).getFilePath());
                    //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });

            }

            @Override
            public void customFun9(ChatDocItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                String path=mList.get(position).getFilePath();//it contain your path of image..im using a temp string..
                String filename=path.substring(path.lastIndexOf("/")+1);
                binding.tvFileNmae.setText(filename);
                binding.llPdf.setOnClickListener(v->{
                    Intent intent = new Intent(context, PDFActivity.class);
                    intent.putExtra("DocPath",mList.get(position).getFilePath());
                    //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });


            }
        };
//                bubbleBackgroundRcv, bubbleBackgroundSend, bubbleElevation);
        chatListView.setAdapter(chatViewListAdapter);
    }


    private void setViewAttributes() {
        setChatViewBackground();
        setInputFrameAttributes();
        setInputTextAttributes();
        setSendButtonAttributes();
        setUseEditorAction();
    }

    private void getChatViewBackgroundColor() {
        backgroundColor = attributes.getColor(R.styleable.ChatView_backgroundColor, -1);
    }

    private void getAttributesForBubbles() {

        float dip4 = context.getResources().getDisplayMetrics().density * 4.0f;
        int elevation = attributes.getInt(R.styleable.ChatView_bubbleElevation, ELEVATED);
        bubbleElevation = elevation == ELEVATED ? dip4 : 0;

        bubbleBackgroundRcv = attributes.getColor(R.styleable.ChatView_bubbleBackgroundRcv, ContextCompat.getColor(context, R.color.white));
        bubbleBackgroundSend = attributes.getColor(R.styleable.ChatView_bubbleBackgroundSend, ContextCompat.getColor(context, R.color.default_bubble_color_send));
    }


    private void getAttributesForInputFrame() {
        inputFrameBackgroundColor = attributes.getColor(R.styleable.ChatView_inputBackgroundColor, -1);
    }

    private void setInputFrameAttributes() {
        inputFrame.setCardBackgroundColor(inputFrameBackgroundColor);
    }

    private void setChatViewBackground() {
        this.setBackgroundColor(backgroundColor);
    }

    private void getAttributesForInputText() {
        setInputTextDefaults();
        if (hasStyleResourceSet()) {
            setTextAppearanceAttributes();
            setInputTextSize();
            setInputTextColor();
            setInputHintColor();
            textAppearanceAttributes.recycle();
        }
        overrideTextStylesIfSetIndividually();
    }

    private void setTextAppearanceAttributes() {
        final int textAppearanceId = attributes.getResourceId(R.styleable.ChatView_inputTextAppearance, 0);
        textAppearanceAttributes = getContext().obtainStyledAttributes(textAppearanceId, R.styleable.ChatViewInputTextAppearance);
    }

    private void setInputTextAttributes() {
        inputEditText.setTextColor(inputTextColor);
        inputEditText.setHintTextColor(inputHintColor);
        inputEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, inputTextSize);
    }

    private void getAttributesForSendButton() {
        sendButtonBackgroundTint = attributes.getColor(R.styleable.ChatView_sendBtnBackgroundTint, -1);
        sendButtonIconTint = attributes.getColor(R.styleable.ChatView_sendBtnIconTint, Color.WHITE);
        sendButtonIcon = attributes.getDrawable(R.styleable.ChatView_sendBtnIcon);
    }

    private void setSendButtonAttributes() {
        actionsMenu.getSendButton().setColorNormal(sendButtonBackgroundTint);
        actionsMenu.setIconDrawable(sendButtonIcon);

        buttonDrawable = actionsMenu.getIconDrawable();
        actionsMenu.setButtonIconTint(sendButtonIconTint);
    }

    private void getUseEditorAction() {
        useEditorAction = attributes.getBoolean(R.styleable.ChatView_inputUseEditorAction, false);
    }

    private void setUseEditorAction() {
        if (useEditorAction) {
            setupEditorAction();
        } else {
            inputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        }
    }

    private boolean hasStyleResourceSet() {
        return attributes.hasValue(R.styleable.ChatView_inputTextAppearance);
    }

    private void setInputTextDefaults() {
        inputTextSize = context.getResources().getDimensionPixelSize(R.dimen.default_input_text_size);
        inputTextColor = ContextCompat.getColor(context, R.color.black);
        inputHintColor = ContextCompat.getColor(context, R.color.main_color_gray);
    }

    private void setInputTextSize() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextSize)) {
            inputTextSize = attributes.getDimensionPixelSize(R.styleable.ChatView_inputTextSize, inputTextSize);
        }
    }

    private void setInputTextColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextColor)) {
            inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        }
    }

    private void setInputHintColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputHintColor)) {
            inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
        }
    }

    private void overrideTextStylesIfSetIndividually() {
        inputTextSize = (int) attributes.getDimension(R.styleable.ChatView_inputTextSize, inputTextSize);
        inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
    }

    private void setupEditorAction() {
        inputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        inputEditText.setImeOptions(EditorInfo.IME_ACTION_SEND);
        inputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    long stamp = System.currentTimeMillis();
                    String message = inputEditText.getText().toString();

                    if (!TextUtils.isEmpty(message)) {
                        sendMessage(message, stamp,ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void setButtonClickListeners() {

        actionsMenu.getSendButton().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (actionsMenu.isExpanded()) {
                    actionsMenu.collapse();
                    return;
                }

                long stamp = System.currentTimeMillis();
                String message = inputEditText.getText().toString();
                if (!TextUtils.isEmpty(message)) {
                    sendMessage(message, stamp,ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                }




            }
        });

        actionsMenu.getSendButton().setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                actionsMenu.expand();
                return true;
            }
        });

        ivCamera.setOnClickListener(view -> attachmentListener.onCamClick(true));
        ivAttachment.setOnClickListener(view -> attachmentListener.onCamClick(false));
        emoji_btn.setOnClickListener(view -> emojiClickListener.onEmojiClick(true));
    }

    private void setUserTypingListener() {
        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {

                    if (!isTyping) {
                        isTyping = true;
                        if (typingListener != null) typingListener.userStartedTyping();
                    }

                    removeCallbacks(typingTimerRunnable);
                    postDelayed(typingTimerRunnable, 1500);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setUserStoppedTypingListener() {

        inputEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (previousFocusState && !hasFocus && typingListener != null) {
                    typingListener.userStoppedTyping();
                }
                previousFocusState = hasFocus;
            }
        });
    }
    private void setUserClickListener(){

        ivRecordVoice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                typingListener.userClick();

            }
        });




    }


    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params) {
        return super.addViewInLayout(child, index, params);
    }

    public String getTypedMessage() {
        return inputEditText.getText().toString();
    }

    public void setTypingListener(TypingListener typingListener) {
        this.typingListener = typingListener;
    }

    public void setOnSentMessageListener(OnSentMessageListener onSentMessageListener) {
        this.onSentMessageListener = onSentMessageListener;
    }

    public void setEmojiClickListener(OnEmojiClickListener emojiClickListener) {
        this.emojiClickListener = emojiClickListener;
    }

    public void setAttachmentListener(OnAttachmentClickListener attachmentListener) {
        this.attachmentListener = attachmentListener;
    }

    private void sendMessage(String message, long stamp,int msgType) {

        ChatMessage chatMessage = new ChatMessage(message, stamp, msgType);
        if (onSentMessageListener != null && onSentMessageListener.sendMessage(chatMessage)) {
            chatViewListAdapter.addMessage(chatMessage);
            chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
            inputEditText.setText("");
        }
    }

    public void addMessage(ChatMessage chatMessage) {
        chatViewListAdapter.addMessage(chatMessage);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void addMessageImg(ChatMessage chatMessage) {
        chatViewListAdapter.addMessage(chatMessage);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void addMessages(ArrayList<ChatMessage> messages) {
        chatViewListAdapter.addMessages(messages);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void removeMessage(int position) {
        chatViewListAdapter.removeMessage(position);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void clearMessages() {
        chatViewListAdapter.clearMessages();
    }

    public EditText getInputEditText() {
        return inputEditText;
    }

    public FloatingActionsMenu getActionsMenu() {
        return actionsMenu;
    }

    public void hideInputLay() {
        inputLayout.setVisibility(View.GONE);
    }

    public void showInputLay() {
        inputLayout.setVisibility(View.VISIBLE);
    }


    public interface TypingListener {

        void userStartedTyping();

        void userStoppedTyping();

        void userClick();

    }

    public interface OnSentMessageListener {
        boolean sendMessage(ChatMessage chatMessage);
    }

    public interface OnAttachmentClickListener {
        void onCamClick(boolean isCam);
    }


    public interface OnEmojiClickListener {
        void onEmojiClick(boolean emoji);
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }

}
