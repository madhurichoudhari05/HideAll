package madhu.dto;


import android.support.annotation.NonNull;

/**
 * Created by abul on 8/12/17.
 */

public class ShayariDetailDto  implements Comparable{
    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
   /* @SerializedName("I")
    @Expose
    private String imageId;
    @SerializedName("PI")
    @Expose
    private String imagePoetId;
    @SerializedName("NE")
    @Expose
    private String imagePoetNameInEng;
    @SerializedName("NH")
    @Expose
    private String imagePoetNameInHin;
    @SerializedName("NU")
    @Expose
    private String imagePoetNameInUrdu;
    @SerializedName("IE")
    @Expose
    private Boolean iE;
    @SerializedName("IH")
    @Expose
    private Boolean iH;
    @SerializedName("IU")
    @Expose
    private Boolean iU;
    @SerializedName("D")
    @Expose
    private String imageDate;
    @SerializedName("SI")
    @Expose
    private Double sI;
    @SerializedName("UE")
    @Expose
    private String urlEng;
    @SerializedName("UH")
    @Expose
    private String urlHindi;
    @SerializedName("UU")
    @Expose
    private String urlUrdu;
    @SerializedName("CI")
    @Expose
    private String imageContentId;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImagePoetId() {
        return imagePoetId;
    }

    public void setImagePoetId(String imagePoetId) {
        this.imagePoetId = imagePoetId;
    }

    public String getImagePoetNameInEng() {
        return imagePoetNameInEng;
    }

    public void setImagePoetNameInEng(String imagePoetNameInEng) {
        this.imagePoetNameInEng = imagePoetNameInEng;
    }

    public String getImagePoetNameInHin() {
        return imagePoetNameInHin;
    }

    public void setImagePoetNameInHin(String imagePoetNameInHin) {
        this.imagePoetNameInHin = imagePoetNameInHin;
    }

    public String getImagePoetNameInUrdu() {
        return imagePoetNameInUrdu;
    }

    public void setImagePoetNameInUrdu(String imagePoetNameInUrdu) {
        this.imagePoetNameInUrdu = imagePoetNameInUrdu;
    }

    public Boolean getiE() {
        return iE;
    }

    public void setiE(Boolean iE) {
        this.iE = iE;
    }

    public Boolean getiH() {
        return iH;
    }

    public void setiH(Boolean iH) {
        this.iH = iH;
    }

    public Boolean getiU() {
        return iU;
    }

    public void setiU(Boolean iU) {
        this.iU = iU;
    }

    public String getImageDate() {
        return imageDate;
    }

    public void setImageDate(String imageDate) {
        this.imageDate = imageDate;
    }

    public Double getsI() {
        return sI;
    }

    public void setsI(Double sI) {
        this.sI = sI;
    }

    public String getUrlEng() {
        return urlEng;
    }

    public void setUrlEng(String urlEng) {
        this.urlEng = urlEng;
    }

    public String getUrlHindi() {
        return urlHindi;
    }

    public void setUrlHindi(String urlHindi) {
        this.urlHindi = urlHindi;
    }

    public String getUrlUrdu() {
        return urlUrdu;
    }

    public void setUrlUrdu(String urlUrdu) {
        this.urlUrdu = urlUrdu;
    }

    public String getImageContentId() {
        return imageContentId;
    }

    public void setImageContentId(String imageContentId) {
        this.imageContentId = imageContentId;
    }

    @Override
    public int compareTo(Object another) {
        String tmpDate = ((ShayariDetailDto) another).imageDate.trim().split("T")[0];
        return this.imageDate.trim().split("T")[0].compareToIgnoreCase(tmpDate);
    }*/
}
