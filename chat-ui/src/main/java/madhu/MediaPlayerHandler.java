package madhu;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by abul on 6/12/17.
 */

public class MediaPlayerHandler {
    public static final int PLAYING = 1;
    public static final int PAUSE = 2;
    public static final int STOP = 3;
    public static final int LOADING = 4;
    public static final int LOADED = 5;
    public static final int LOAD_FAIL = 6;
    private int currentState = 0;

    private static final String LOG_TAG = "player";
    private String url = "";
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;
    private SeekListner listner;
    private static MediaPlayerHandler mediaHandler;
    Timer mTimer;
    boolean isPlaying;
    private int handlerPosition=-1;


    public MediaPlayerHandler() {
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mTimer = new Timer();
    }

    public static MediaPlayerHandler getInstance() {
        if (mediaHandler == null) {
            mediaHandler = new MediaPlayerHandler();
        }
        return mediaHandler;
    }

    public void setSeekLisner(String url, int position, SeekListner lisner) {
        this.url = url;
        if(handlerPosition!=position){
            if(this.listner!=null){
                this.listner.invalidatePrevious();
            }
            handlerPosition=position;
            this.listner = lisner;
            stopPlaying();
        }


    }



    public void startPlaying() {
        if (currentState == LOADING || currentState == LOADED) {
            return;
        } else if (currentState == PLAYING) {
            pausePlaying();
        } else if (currentState == PAUSE) {
            isPlaying = true;
            mPlayer.start();
            setCurrentState(PLAYING);
        } else {
            setCurrentState(LOADING);
            try {
                mPlayer = new MediaPlayer();
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.setDataSource(url);
                mPlayer.prepareAsync();
                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(final MediaPlayer player) {
                        setCurrentState(LOADED);
                        player.start();
                        Log.e(LOG_TAG, player.getDuration() + "");
                        listner.setSeekBarMaxLength(player.getDuration());
                        listner.setTotalLength(player.getDuration());
                        setCurrentState(PLAYING);
                        isPlaying = true;
                        mTimer.schedule(new MyTimerTask(), 2, 10);
                    }

                });

                mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        setCurrentState(STOP);
                    }
                });
//                Log.e(LOG_TAG, getDuration(url) + "");
            } catch (IOException e) {
                setCurrentState(LOAD_FAIL);
                Log.e(LOG_TAG, "prepare() failed");
            }
        }

    }

    public void stopPlaying() {
        mPlayer.stop();
        mPlayer.release();
        setCurrentState(STOP);
        reStartTimer();
    }

    public void pausePlaying() {
        mPlayer.pause();
        setCurrentState(PAUSE);
        reStartTimer();
    }

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            try {
                if (isPlaying/*mPlayer.isPlaying()*/) {
                    listner.setSeekBarCurrentProgress(mPlayer.getCurrentPosition());

                }
            } catch (Exception e) {
            }

        }
    }

    ;

    private void reStartTimer() {
        isPlaying = false;
    }

    private void setCurrentState(int state) {
        listner.playStatus(state);
        currentState = state;
    }

   /* public void startRecord(boolean start) {
        if (start) {
            mStartRecording = true;
            startRecording();
        } else {
            mStartRecording = false;
            stopRecording();
        }
    }

    public void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(url);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    public void stopRecording() {
        mRecorder.STOP();
        mRecorder.release();
        mRecorder = null;
    }*/


    public void onPause() {
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void setUsingSeekBar(int progress, boolean fromUser) {
        if (mPlayer != null && fromUser) {
            reStartTimer();
            mPlayer.seekTo(progress);
            isPlaying = true;

        }
    }


    public interface SeekListner {
        void setSeekBarMaxLength(int setLength);

        void setSeekBarCurrentProgress(int length);

        void playStatus(int isPlaying);

        void invalidatePrevious();

        void setTotalLength(int duration);
    }
}