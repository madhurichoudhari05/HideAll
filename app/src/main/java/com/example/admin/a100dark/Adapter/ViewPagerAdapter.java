package com.example.admin.a100dark.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.widget.Toast;

import com.example.admin.a100dark.Fragment.CallFragment;
import com.example.admin.a100dark.Fragment.ChatFragment;
import com.example.admin.a100dark.Fragment.ContainerCallFragment;
import com.example.admin.a100dark.Fragment.ContainerChatFragment;
import com.example.admin.a100dark.Fragment.FirstCallFragment;
import com.example.admin.a100dark.Fragment.NewUserFragment;
import com.example.admin.a100dark.Fragment.StatusFragment;
import com.example.admin.a100dark.Fragment.cam_frgament;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

/**
 * Created by Admin on 10/31/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    Drawable myDrawable;
    private Context mcontext;

    public ViewPagerAdapter(FragmentManager fm ,Context  mcontext) {
        super(fm);
       this. mcontext=mcontext;

    }
    @Override
    public Fragment getItem(int position) {
        if(position==0) {
            /*return new cam_frgament();*/
            if(!CommonUtils.getPreferencesBoolean(mcontext, AppConstants.FIRST_TIME_LOGIN))
                return new ContainerChatFragment();
                //return new NewUserFragment();

            else
                return new ChatFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 1;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return "Chats";
            /*case 1:

                return "Chats";
            case 2:
                return "";
*/
            default:return "";
        }
    }
}
