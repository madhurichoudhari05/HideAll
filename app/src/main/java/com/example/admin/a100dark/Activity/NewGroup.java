package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.ConatctAdapter;
import com.example.admin.a100dark.Adapter.NewGroupAdapter;
import com.example.admin.a100dark.Model.Contact_model;
import com.example.admin.a100dark.Model.NewGroup_model;
import com.example.admin.a100dark.R;

import java.util.ArrayList;

/**
 * Created by Admin on 11/9/2017.
 */

public class NewGroup extends AppCompatActivity {


    RecyclerView newgrouptRecylerviewl;

    ArrayList<NewGroup_model> arrayList= new ArrayList<>();
    NewGroupAdapter newGroupAdapter;
    private Context context;
    ImageView arrowmessenger;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.newgroup);

        arrowmessenger= (ImageView)findViewById(R.id.arrowmessenger);
        arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(NewGroup.this,MainActivity.class));

            }
        });
        context=NewGroup.this;

        newgrouptRecylerviewl = (RecyclerView)findViewById(R.id.newgrouptRecylerview);

        newgrouptRecylerviewl.setFocusable(false);

        newgrouptRecylerviewl.setNestedScrollingEnabled(false);

        Toast.makeText(context, "Checked", Toast.LENGTH_SHORT).show();

        init();

        newGroupAdapter = new NewGroupAdapter(arrayList,context);
        newgrouptRecylerviewl.setLayoutManager(new LinearLayoutManager(context));
        newgrouptRecylerviewl.setAdapter(newGroupAdapter);
    }

    public void init()

    {
        for (int i = 0 ; i<10;i++) {
            NewGroup_model newGroup_model = new NewGroup_model("User name ","Today 9:00 a.m.",R.drawable.images);
            arrayList.add(newGroup_model);

        }
    }

    }



