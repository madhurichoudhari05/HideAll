package com.example.admin.a100dark.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.a100dark.R;

/**
 * Created by Abdul on 5/8/2018.
 */

public class ChangeStatusAdapter extends RecyclerView.Adapter {

    Context context;
    String[] all_status;

    public ChangeStatusAdapter(Context context, String[] all_status) {
        this.context = context;
        this.all_status = all_status;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.change_status_items, null, false);

        return new StatusViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        StatusViewHolder statusViewHolder = (StatusViewHolder) holder;

        statusViewHolder.textView.setText(all_status[position]);
    }

    @Override
    public int getItemCount() {
        return all_status.length;
    }

    class StatusViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public StatusViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.old_status);
        }
    }

}
