package com.example.admin.a100dark.Model;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Admin on 11/10/2017.
 */

public class NewBroadCast_model {
    public String getNewBroadCast_name() {
        return NewBroadCast_name;
    }

    public NewBroadCast_model(String newBroadCast_name, String newBroadCast_status, int newBroadCast_image) {
        NewBroadCast_name = newBroadCast_name;
        NewBroadCast_status = newBroadCast_status;
        NewBroadCast_image = newBroadCast_image;
    }

    public void setNewBroadCast_name(String newBroadCast_name) {
        NewBroadCast_name = newBroadCast_name;
    }

    public String getNewBroadCast_status() {
        return NewBroadCast_status;
    }

    public void setNewBroadCast_status(String newBroadCast_status) {
        NewBroadCast_status = newBroadCast_status;
    }

    public int getNewBroadCast_image() {
        return NewBroadCast_image;
    }

    public void setNewBroadCast_image(int newBroadCast_image) {
        NewBroadCast_image = newBroadCast_image;
    }

    private String NewBroadCast_name;
    private String NewBroadCast_status;
    private int NewBroadCast_image;







}
