package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.example.admin.a100dark.R;

import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

public class EditStatusActivity extends AppCompatActivity {


    EditText userStatusEdit;

    private RelativeLayout cancelRelative, okRelative;
    private String selectedName;
    ImageView emojiIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_status);
        userStatusEdit = findViewById(R.id.userStatusEdit);


        cancelRelative = findViewById(R.id.cancelRelative);
        okRelative = findViewById(R.id.okRelative);
        emojiIcon = findViewById(R.id.emojiStatusIcon);


        userStatusEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return true;
                }
                return false;
            }
        });

        final View rootView = findViewById(R.id.statusRootView);




       // userStatusEdit.setText(CommonUtils.getPreferencesString(EditUsernameActivity.this, AppConstants.USER_NAME));

        cancelRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        okRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedName = userStatusEdit.getText().toString();

                CommonUtils.savePreferencesString(EditStatusActivity.this, AppConstants.USER_NAME, selectedName);
                finish();
            }
        });

        emojiIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {

        iconToBeChanged.setImageResource(drawableResourceId);
    }
}
