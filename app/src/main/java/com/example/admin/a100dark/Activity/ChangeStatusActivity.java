package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.admin.a100dark.Adapter.ChangeStatusAdapter;
import com.example.admin.a100dark.R;

public class ChangeStatusActivity extends AppCompatActivity {


    RecyclerView statusRecycler;
    ImageView editStatus;

    String [] all_status={"Available","Away","Busy","At Work","Home","At the movies","In a meating","Sleeping",
            "Urgent Calls only","Gym","Office","Dinner"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_status);

        statusRecycler = findViewById(R.id.statusRecycler);
        editStatus = findViewById(R.id.editStatus);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChangeStatusActivity.this);
        statusRecycler.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(statusRecycler.getContext(),
                linearLayoutManager.getOrientation());
        statusRecycler.addItemDecoration(dividerItemDecoration);

        ChangeStatusAdapter changeStatusAdapter = new ChangeStatusAdapter(ChangeStatusActivity.this,all_status);
        statusRecycler.setAdapter(changeStatusAdapter);

        editStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ChangeStatusActivity.this, EditStatusActivity.class));
            }
        });

        findViewById(R.id.statusBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
