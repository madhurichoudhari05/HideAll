package com.example.admin.a100dark.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.ChatActivirtyMesssage;
import com.example.admin.a100dark.Activity.MainActivity;
import com.example.admin.a100dark.Activity.PopUpUserInfo;
import com.example.admin.a100dark.Activity.SeachhActivity;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.chat_model;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.db.entities.ChatMessageDto;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.chat.app.retofit.ApiInterface;
import com.example.admin.a100dark.chat.app.ui.ChatActivity;
import com.example.admin.a100dark.chat.app.ui.FcmMessengerViewModel;
import com.example.admin.a100dark.chat.dependency.handler.ChstRetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 11/3/2017.
 */


public class SearchAdapter extends  RecyclerView.Adapter<SearchAdapter.ViewHolder> implements Filterable {
    Dialog mDialog;
    ImageView pro_pic;
    TextView userName;
    boolean isImageFitToScreen;
    private int user_id = 0;
    List<AllUserListDetailsModel> arrayList = new ArrayList<>();
    Context context;
    private ApiInterface mInterface;

    private FcmMessengerViewModel fcmVM;
    private ValueFilter valueFilter;
    private List<AllUserListDetailsModel> mStringFilterList;
    private List<AllUserListDetailsModel> bothList;
    private AppDatabase mAppDb;




    public SearchAdapter(List<AllUserListDetailsModel> arrayList, Context activity ,List<AllUserListDetailsModel> bothList) {
        this.arrayList = arrayList;
        this.bothList = bothList;
        this.context = activity;
        mStringFilterList=arrayList;
        mInterface = ChstRetrofitHandler.getInstance().getApi();
        this.fcmVM=fcmVM;
        mAppDb = MyApplication.getDb();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.searchchat_item, parent, false);
        ViewHolder recyclerViewHolder = new ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AllUserListDetailsModel modelDetail = arrayList.get(position);

       // Toast.makeText(context, "bothlist::"+bothList.size(), Toast.LENGTH_SHORT).show();

        user_id = CommonUtils.getIntPreferences(context, AppConstants.USER_ID);
        if (!TextUtils.isEmpty(modelDetail.getUserName()) && modelDetail.getUserName() != null) {
            holder.userName.setText(CommonUtils.NameCaps(modelDetail.getUserName()));

        } else {
            String name="No name";
            if(modelDetail.getUserName()!=null && !modelDetail.getUserName().trim().equals("")){
                name=modelDetail.getUserName();
            }
            holder.userName.setText(name+ (modelDetail.getUserMobile()));
            holder.userName.setTextColor(context.getResources().getColor(R.color.redcolor));
        }

        if (!TextUtils.isEmpty(modelDetail.getUserMobile()) && modelDetail.getUserMobile() != null) {
            holder.status.setText(modelDetail.getUserMobile());
        } else {
            holder.status.setText("No Number");
        }
        if (!TextUtils.isEmpty(modelDetail.getUserPic()) && modelDetail.getUserPic() != null) {
            Picasso.with(context).load(modelDetail.getUserPic()).into(holder.profileImage);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<UserStatusDto> users = new ArrayList<>();
                        for (AllUserListDetailsModel model :arrayList) {

                            users.add(new UserStatusDto(
                                    model.getId() + "",
                                    model.getUserName() + "",
                                    model.getDeviceToken() + "",
                                    true,
                                    true,
                                    model.getUserPic() + "",
                                    ""
                            ));

                        }


                    new Thread(new Runnable() {
                        @Override
                        public void run() {
//                                mAppDb.userDao().nukeTable();
                            mAppDb.userDao().insertAll(users);
                        }
                    }).start();


                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra(AppConstants.RECEIVER__USER_NAME2, modelDetail.getUserName());
                intent.putExtra(AppConstants.RECEIVER__PIC, modelDetail.getUserPic());
                intent.putExtra(AppConstants.RECEIVER_MOBILE, modelDetail.getUserMobile());
                intent.putExtra(AppConstants.RECEIVER_USER_ID, modelDetail.getId());
                context.startActivity(intent);
                ((Activity)context).finish();


                // (Activity)context.fi

            }
        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(context);
                mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
                mDialog.setCanceledOnTouchOutside(true);
                mDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mDialog.getWindow().setGravity(Gravity.CENTER);
                WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
                lp.dimAmount = 0.85f;
                mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.getWindow();
                mDialog.getWindow().setAttributes(lp);
                View dialoglayout = inflater.inflate(R.layout.popup_profile_pic, null);
                mDialog.setContentView(dialoglayout);
                pro_pic = (ImageView) mDialog.findViewById(R.id.popup_pro_pic);
                userName = (TextView) mDialog.findViewById(R.id.userName);
                if (!TextUtils.isEmpty(modelDetail.getUserPic()) && modelDetail.getUserPic() != null) {

                    Picasso.with(context).load(modelDetail.getUserPic()).into(pro_pic);
                }
                if (!TextUtils.isEmpty(modelDetail.getUserName()) && modelDetail.getUserName() != null) {
                    userName.setText(CommonUtils.NameCaps(modelDetail.getUserName()));

                }
                // pro_pic.setImageResource(R.id.);
                mDialog.show();
                pro_pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, PopUpUserInfo.class);
                        intent.putExtra("USERNAME", modelDetail.getUserName());
                        intent.putExtra("PROFILEPIC", modelDetail.getUserPic());
                        mDialog.dismiss();
                        context.startActivity(intent);
                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, status, timing,count;
        LinearLayout linearLayout;
        CircleImageView profileImage;
        public ViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
            userName = (TextView) itemView.findViewById(R.id.chat_user_name);
            status = (TextView) itemView.findViewById(R.id.chat_status);
            timing = (TextView) itemView.findViewById(R.id.time);
            profileImage = (CircleImageView) itemView.findViewById(R.id.chat_profile_image);
            count= (TextView) itemView.findViewById(R.id.count);
        }
    }

    @Override
    public Filter getFilter() {
        if(valueFilter==null) {

            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }
    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<AllUserListDetailsModel> filterList=new ArrayList<AllUserListDetailsModel>();
                for(int i=0;i<bothList.size();i++){
                    if((bothList.get(i).getUserName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        AllUserListDetailsModel contacts = new AllUserListDetailsModel();
                        contacts.setUserName(bothList.get(i).getUserName());
                       contacts.setUserMobile(bothList.get(i).getUserMobile());
                        filterList.add(contacts);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=bothList.size();
                results.values=bothList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            arrayList=(ArrayList<AllUserListDetailsModel>) results.values;
            notifyDataSetChanged();
        }
    }
}

