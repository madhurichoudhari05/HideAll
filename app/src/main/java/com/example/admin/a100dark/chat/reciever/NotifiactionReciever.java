package com.example.admin.a100dark.chat.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.db.entities.ChatMessageDto;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.chat.app.ui.ChatActivity;
import com.example.admin.a100dark.chat.dependency.handler.ExecutorHandler;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.chat.utils.AppUtils;
import com.example.admin.a100dark.chat.utils.NotificationDto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;

import co.intentservice.chatui.models.ChatMessage;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class NotifiactionReciever extends BroadcastReceiver implements IConstants.IFcm, IConstants.INotification {

    ExecutorService mExecutor;
    AppDatabase mAppDb;
    private List<UserStatusDto> userDetail;
    private Flowable<List<UserStatusDto>> user;
    private boolean isDBUpdate;
    private Flowable<List<ChatMessageDto>> chat;
    private List<ChatMessageDto> chatList = new ArrayList<>();
    private boolean isMsgRead;


    @Override
    public void onReceive(Context context, Intent intent) {
        /*here we get the json we send form the user side*/
        String msg = intent.getStringExtra(IConstants.IApp.PARAM_1);
        //Toast.makeText(context, "notification", Toast.LENGTH_SHORT).show();
        String reply = intent.getStringExtra(IConstants.IApp.PARAM_2);
        if (reply != null && !reply.trim().equals("")) {
            sendNotification(context, reply, IConstants.IFcm.FCM_REPLY);
            return;
        }

        isDBUpdate = false;
        isMsgRead = false;
        mAppDb = MyApplication.getDb();
        mExecutor = ExecutorHandler.getInstance().getExecutor();

        if (user == null) {

            user = mAppDb.userDao().getAllRx();
            user.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(responseBody -> {
                        if (isDBUpdate == false) {
                            userDetail = responseBody;
                            handleMsg(context, msg);
                            isDBUpdate = true;
                        }

                    }, e -> {
                    });

            chat = mAppDb.chatDao().loadMsgByReadStatusRx(false);
            chat.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(responseBody -> {
                        if (!isMsgRead) {
                            chatList.clear();
                            chatList.addAll(responseBody);
                            handleMsg(context, msg);
                            isMsgRead = true;
                        }

                    }, e -> {
                    });

        } else {
            handleMsg(context, msg);
        }

    }

    private void handleMsg(Context context, String msg) {
        if (msg != null) {
            ChatMessageDto chat = new Gson().fromJson(msg, ChatMessageDto.class);

            handleChatMessage(context, chat);

        }
    }


    /****************
     * Handling Type of Messages
     * ******************/
    private void handleChatMessage(Context context, ChatMessageDto chat) {
        saveMessage(chat);
        if(!checkAvailable(chat.getToId())){
            saveUser(chat);
        }

        String notMsg = "";
        ChatMessageDto cloned = chat.clone();
        if (chatList.size() > 0) {
            for (ChatMessageDto dto : chatList) {
                notMsg = dto.getToName() + " : " + dto.getMsg() + "\n" + notMsg;
            }
            notMsg += chat.getMyName() + " : " + chat.getMsg();

            cloned.setMsg(notMsg);
        }

        if (AppUtils.isAppRunning(context)) {
            Log.e("package", AppUtils.isRunning(context) + "\n" + ChatActivity.class.getSimpleName());
            if (!AppUtils.isRunning(context).contains(ChatActivity.class.getSimpleName())) {
                sendNotification(context, new Gson().toJson(cloned), chat.getMsgType());
            }
        } else {
            sendNotification(context, new Gson().toJson(cloned), chat.getMsgType());
        }
    }

    /****************
     * Save Data to DB
     * ***************/
    private void saveMessage(ChatMessageDto chat) {
        mExecutor.execute(() -> mAppDb.chatDao().insert(chat));
    }

    private void saveUser(ChatMessageDto chat) {
        UserStatusDto table = new UserStatusDto(
                chat.getMyId(),
                chat.getMsg(),
                true,
                MY_APPROVAL,
                chat.getMyName(),
                chat.getMyPic()
        );
        mExecutor.execute(() -> mAppDb.userDao().insert(table));
    }

    private boolean checkAvailable(String userId) {
        boolean isUserValid = false;
        for (UserStatusDto dto : userDetail) {
            if (dto.getToId().equals(userId)) {
                isUserValid = true;
            }
        }
        return isUserValid;
    }

    /*****************************
     * showing notification on message recieced
     * **************************/
    private void sendNotification(Context context, String remoteMessage, int type) {


        int count = type;

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        NotificationDto dto = new NotificationDto();
        dto.setType(type);
        dto.setMsg(remoteMessage);
        dto.setDate(formattedDate);
        dto.setReadStatus(0);
        dto.setId(0);

        AppUtils.showNotification2(context, dto, count);
    }


}
