package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Fragment.FragmentSetting;
import com.example.admin.a100dark.ProfileActivty;
import com.example.admin.a100dark.R;

/**
 * Created by Admin on 11/9/2017.
 */

public class Setting extends AppCompatActivity {
    LinearLayout linearAccount, linearLayoutChat, linearLayoutNotication,linearLayoutDatauseg, linearLayoutContacts, linearLayouthelp;
    LinearLayout arrowmessenger,profilelayout;
    ImageView back;
    TextView toolTitle;

    LinearLayout containerLayout;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    Fragment settingFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        containerLayout= (LinearLayout) findViewById(R.id.container);

        fragmentManager=getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();
        settingFragment=new FragmentSetting();
        fragmentTransaction.replace(R.id.container,settingFragment);
        fragmentTransaction.commit();


       /* linearAccount = (LinearLayout)findViewById(R.id.linearAccount);
       // arrowmessenger = (LinearLayout) findViewById(R.id.arrowmessenger);
        linearLayoutChat = (LinearLayout)findViewById(R.id.linearChat);
        linearLayoutNotication = (LinearLayout)findViewById(R.id.linear_layoutNotication);
        linearLayoutDatauseg = (LinearLayout)findViewById(R.id.linear_layoutDatausage);
        linearLayoutContacts = (LinearLayout)findViewById(R.id.linear_layoutConatcts);
        linearLayouthelp = (LinearLayout)findViewById(R.id.linear_layoutHelp);
        profilelayout = (LinearLayout)findViewById(R.id.profilelayout);
        back= (ImageView) findViewById(R.id.toolbar_back);
        toolTitle= (TextView) findViewById(R.id.toolbar_title);
        toolTitle.setText("Setting");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        profilelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Setting.this,ProfileActivty.class));


            }
        });
*/
      /*  linearLayoutChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Setting.this,Chats.class));

            }
        });



        linearLayoutNotication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Setting.this,Notitication.class));

            }
        });


        linearLayoutDatauseg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Setting.this,DataUsage.class));

            }
        });



        linearLayoutContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Setting.this,Contacts.class));

            }
        });
        linearLayouthelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Setting.this,Help.class));

            }
        });
*/

       /* arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Setting.this,MainActivity.class));

            }
        });
*/

  /*      linearAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Setting.this, AccountActivity.class));

            }
        });*/
    }
}
