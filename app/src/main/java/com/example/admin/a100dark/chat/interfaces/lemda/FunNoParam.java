package com.example.admin.a100dark.chat.interfaces.lemda;

/**
 * Created by abul on 12/12/17.
 */

@FunctionalInterface
public interface FunNoParam extends Fun{
    void done();
}
