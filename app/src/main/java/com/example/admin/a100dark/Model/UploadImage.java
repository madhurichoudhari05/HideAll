package com.example.admin.a100dark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Madhuri on 4/4/2018.
 */

public class UploadImage {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("allUser")
    @Expose
    private String allUser;
    @SerializedName("allImage")
    @Expose
    private List<UploadImageDetails> allImage = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getAllUser() {
        return allUser;
    }

    public void setAllUser(String allUser) {
        this.allUser = allUser;
    }

    public List<UploadImageDetails> getAllImage() {
        return allImage;
    }

    public void setAllImage(List<UploadImageDetails> allImage) {
        this.allImage = allImage;
    }

}
