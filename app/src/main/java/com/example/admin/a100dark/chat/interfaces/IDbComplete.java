package com.example.admin.a100dark.chat.interfaces;

/**
 * Created by abul on 28/11/17.
 */

public interface IDbComplete<T> {

    void onComplete(T t);
}
