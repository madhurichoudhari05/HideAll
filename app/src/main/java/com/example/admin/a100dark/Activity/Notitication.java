package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.admin.a100dark.R;

public class Notitication extends AppCompatActivity {
    LinearLayout arrowmessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notitication);
        arrowmessenger = (LinearLayout)findViewById(R.id.arrowmessenger);
        arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Notitication.this,Setting.class));


            }
        });
    }
}
