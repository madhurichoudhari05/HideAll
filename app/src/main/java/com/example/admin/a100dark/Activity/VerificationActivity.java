package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.admin.a100dark.Model.OTPVerifyModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.Pinview;

import java.util.List;

import retrofit2.Callback;

public class VerificationActivity extends AppCompatActivity {

    private Button button;
    private Pinview tvOTP;
    private Context context;
    private int  otp=0;
    private int  user_id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        context=VerificationActivity.this;
        button=(Button)findViewById(R.id.verify_account);
        tvOTP=(Pinview) findViewById(R.id.pinview1OTP);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        otp=CommonUtils.getIntPreferences(context, AppConstants.SIGNUP_OTP);
        user_id=CommonUtils.getIntPreferences(context, AppConstants.USER_ID);
        tvOTP.setValue(otp+"");


//        getSupportActionBar().setTitle("");
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                calOTPVerifyApi();
            }
        });




    }
    private void calOTPVerifyApi() {
        CommonUtils.showProgress(context);

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<List<OTPVerifyModel>> call = service.getOTPVerify(user_id,otp);
        call.enqueue(new Callback<List<OTPVerifyModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<OTPVerifyModel>> call, retrofit2.Response<List<OTPVerifyModel>> response) {
                CommonUtils.dismissProgress();
                List<OTPVerifyModel> registerList = response.body();

                if (registerList!=null&&registerList.size()>0) {
                    if(registerList.get(0).getStatus()) {
                        startActivity(new Intent(VerificationActivity.this,ProfileInfo.class));
                        finish();
                        CommonUtils.snackBar(registerList.get(0).getMessage(), button);
                    }

                } else {
                    CommonUtils.snackBar(registerList.get(0).getMessage(), button);
                }

                //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<List<OTPVerifyModel>> call, Throwable t) {
            }
        });
    }


}
