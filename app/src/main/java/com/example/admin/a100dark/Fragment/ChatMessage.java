package com.example.admin.a100dark.Fragment;

/**
 * Created by Admin on 11/2/2017.
 */

public class ChatMessage {

    public boolean left;

    public String message;

    public ChatMessage(boolean left, String message) {
        super();
        this.left = left;
        this.message = message;
    }
}
