package com.example.admin.a100dark.Model;

/**
 * Created by Admin on 11/1/2017.
 */

public class StatusModel {

    private int profileImage;
    private String userName;
    private String statusTiming;


    public StatusModel(int profileImage, String userName, String statusTiming) {

        this.profileImage = profileImage;

        this.userName = userName;

        this.statusTiming = statusTiming;
    }
    public int getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(int profileImage) {
        this.profileImage = profileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatusTiming() {
        return statusTiming;
    }

    public void setStatusTiming(String statusTiming) {
        this.statusTiming = statusTiming;
    }
}
