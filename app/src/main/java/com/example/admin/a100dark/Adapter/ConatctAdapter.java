package com.example.admin.a100dark.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.StatusModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 11/8/2017.
 */


public class ConatctAdapter extends  RecyclerView.Adapter<ConatctAdapter.ViewHolder> {
    ArrayList<AllUserListDetailsModel> arrayList = new ArrayList<>();
    Context context;

    public ConatctAdapter(ArrayList<AllUserListDetailsModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }
    @Override
    public ConatctAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cantact_item, parent, false);
        ConatctAdapter.ViewHolder recyclerViewHolder = new ConatctAdapter.ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(ConatctAdapter.ViewHolder holder, int position) {
        AllUserListDetailsModel AllUserListDetailsModel = arrayList.get(position);
        holder.userName.setText(CommonUtils.NameCaps(AllUserListDetailsModel.getUserName()));
        holder.timing.setText(AllUserListDetailsModel.getMainStatus());

        if(!TextUtils.isEmpty(AllUserListDetailsModel.getUserPic())) {

            Picasso.with(context).load(AllUserListDetailsModel.getUserPic()).into(holder.profileImage);
        }
       /* if (position == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#00BFFF"));
        } else if (position == 1) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#E85E7F"));
        } else if (position == 2) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#0abc70"));
        } else if (position % 3 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#521155"));
        } else if (position % 4 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#f5b022"));
        } else if (position % 5 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#267dc5"));
        }*/
    }
    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, timing;
        LinearLayout linearLayout;
        ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.contact_linear_layout);
            userName = (TextView) itemView.findViewById(R.id.username);
            timing = (TextView) itemView.findViewById(R.id.timing);
            profileImage = (ImageView) itemView.findViewById(R.id.profilenew);


        }
    }
}

