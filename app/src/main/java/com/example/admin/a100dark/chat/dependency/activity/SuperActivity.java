package com.example.admin.a100dark.chat.dependency.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.admin.a100dark.Activity.CommonBaseActivity;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.retofit.FcmApi;
import com.example.admin.a100dark.chat.app.retofit.model.PermissionDto;
import com.example.admin.a100dark.chat.dependency.dialog.ProgressDialogFragment;
import com.example.admin.a100dark.chat.dependency.handler.ExecutorHandler;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.chat.interfaces.lemda.FunNoParam;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import static com.example.admin.a100dark.chat.interfaces.IConstants.IApp.PARAM_1;


/**
 * Created by abul on 29/3/17.
 */
public abstract class SuperActivity extends CommonBaseActivity implements ServiceConnection,IConstants {

    protected AppDatabase mAppDb;
    protected ExecutorService mExecutor;
    protected FcmApi mApi;

    protected Context mContext;
    protected Activity mActivity;
    protected View mView;
    protected ProgressDialogFragment mProgress;
    protected String TAG = "";
    protected FragmentManager mFragManager;
    protected FunNoParam mPogressStartLisner;
    protected FunNoParam mPogressStopListener;
    protected List<PermissionDto> mPermission=new ArrayList<>();


    //TODO  for Video

    public static void start(Activity activity, Class<?> concreteClass, @Nullable Bundle bundle) {
        Intent intent = new Intent(activity, concreteClass);
        if (bundle != null) {
            intent.putExtra(PARAM_1, bundle);
        }
        activity.startActivity(intent);
    }

    public View inflateLayout(int layout, ViewGroup group, boolean bool){
        LayoutInflater inflater= (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layout, group,bool);
    }

    public ViewDataBinding inflateLayoutBinging(int layout, ViewGroup group, boolean bool){
        LayoutInflater inflater= (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        return DataBindingUtil.inflate(inflater, layout, group, bool);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActivity = this;

       // getApplicationContext().bindService(new Intent(this, SinchService.class), this, BIND_AUTO_CREATE);



        mFragManager = getSupportFragmentManager();
        TAG = this.getClass().getSimpleName();
        mAppDb= MyApplication.getDb();
        /*MadhuComment*/
       // mApi= FCMRetrofitHandler.getInstance().getApi();
        mExecutor= ExecutorHandler.getInstance().getExecutor();
        mView=  findViewById(android.R.id.content);
    }

    public void setmProgressListener(FunNoParam mPogressStartLisner,FunNoParam mPogressStopLisner) {
        this.mPogressStartLisner = mPogressStartLisner;
        this.mPogressStopListener=mPogressStopLisner;
    }

    /*************
     * show toast
     * ******************/
    public void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public void showToastTesting(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    /*************
     * show toast Long
     * ******************/
    protected void showToastL(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    /****************
     * Snack bar
     * ***********/
    public void showSnackBar(String msg) {
        final Snackbar mySnackbar = Snackbar.make(mView,
                msg, Snackbar.LENGTH_LONG);
        mySnackbar.setAction("Close", view->mySnackbar.dismiss());

        // Changing message text color
        mySnackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = mySnackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorPrimaryDark));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);

        mySnackbar.show();
    }


    /*************
     * print log
     * ******************/
    public void printLog(String msg) {
        Log.e(TAG, msg);
    }


    /*************
     * show mProgress
     * ******************/
    public void showProgress(String msg) {

        if(mPogressStartLisner!=null){
            mPogressStartLisner.done();
            return;
        }
        if (mProgress == null) {
            mProgress = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG);
            if (mProgress == null) {
                mProgress = new ProgressDialogFragment().newInstance(msg);
            }
        }

        if (!mProgress.isVisible()) {
            try {
                mProgress.show(getSupportFragmentManager(), TAG);
            } catch (Exception e) {
//                showToast("Come Back Later");
                e.printStackTrace();
            }
        }

    }


    /*************
     * dismiss mProgress
     * ******************/
    public void dismissProgress() {
        if(mPogressStopListener!=null){
            mPogressStopListener.done();
            return;
        }
        if (mProgress == null) {
            mProgress = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG);
        }
        if (mProgress != null) {
            try {
                getSupportFragmentManager().beginTransaction().remove(mProgress).commitAllowingStateLoss();
            } catch (Exception e) {
            }
//            mProgress.dismissAllowingStateLoss();
        }
    }


    /****************
     * permissions
     * ****************/
    protected boolean isPerGiven(String per) {
        if (ContextCompat.checkSelfPermission(mActivity, per)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    protected boolean isPermissionRequired(String[] requestPer, int reqId) {
        List<String> requiredPerm = new ArrayList<>();
        for (String per : requestPer) {
            if (ContextCompat.checkSelfPermission(mActivity, per) != PackageManager.PERMISSION_GRANTED) {
                requiredPerm.add(per);
            }
        }

        if (requiredPerm.size() > 0) {
            String[] perArray = new String[requiredPerm.size()];
            perArray = requiredPerm.toArray(perArray);
            ActivityCompat.requestPermissions(mActivity, perArray, reqId);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        boolean isCompGranted=true;
        for(PermissionDto perm:mPermission){
            if (!isPerGiven(perm.getPermission())) {
                if(perm.isCompalsary()){
                    isCompGranted=false;
                    checkPermission(requestCode);
                    break;
                }else {
                    perm.setGranted(true);
                }
            }
        }
        if(isCompGranted){
            List<String> requiredPerm = new ArrayList<>();
            String[] per = null;
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    requiredPerm.add(permissions[i]);
                }
                per = new String[requiredPerm.size()];
            }
            userResponse(requiredPerm.toArray(per), requestCode);
        }

    }

    protected void checkPermission(int reqId){

        List<String> permissions = new ArrayList<>();

        for(PermissionDto perm:mPermission){
            if (!isPerGiven(perm.getPermission())) {
                permissions.add(perm.getPermission());
            }
        }


        if (permissions.size() >= 1) {
            String[] arr = new String[permissions.size()];
            permissions.toArray(arr);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                isPermissionRequired(arr, reqId);
            } else {
                userResponse(null,reqId);
            }
        } else {
            userResponse(null,reqId);
        }
    }

    protected void userResponse(String[] permission, int reqId) {


    }


    /***************
     Network check
     *************/
    protected boolean isNetworkAvailable(Context context) {
        if (isNetworkAvailableWithoutToast(context)) {
            return true;
        } else {
            showToast(INetwork.NETWORK_ERROR);
            return false;
        }
    }

    protected boolean isNetworkAvailableWithoutToast(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }

//TODO for video




    protected void onServiceConnected() {
        // for subclasses
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }




}
