package com.example.admin.a100dark.Activity;

import android.app.Dialog;
import android.app.Fragment;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.ViewPagerAdapter;
import com.example.admin.a100dark.Fragment.CallFragment;
import com.example.admin.a100dark.Fragment.ChatFragment;
import com.example.admin.a100dark.Fragment.ContainerChatFragment;
import com.example.admin.a100dark.Fragment.NewUserFragment;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.ui.FcmMessengerViewModel;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

public class MainActivity extends AppCompatActivity {


    ViewPagerAdapter viewPagerAdapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    Toolbar toolBar;
    TextView setting, newgroup, newbroadcast, whatsappweb, starredmessage;
    ImageView search;
    ImageView more;
    FloatingActionButton fab, fabedit;
    private Context context;

    Dialog mDialog;

    private int[] tabIcons = {
            R.drawable.camera,

    };


    public static Intent getPendingIntent(Context activity, int type) {
        Intent i = new Intent(activity, MainActivity.class);
        i.putExtra(IConstants.IApp.PARAM_1, type);
        return i;
    }

    int noOfReq = 0, noOfChat = 0, total = 0;
    private FcmMessengerViewModel messenger;

    private void setObserver() {
        if (messenger == null) {
            messenger = ViewModelProviders.of(this).get(FcmMessengerViewModel.class);

            messenger.getUserAppList(false, IConstants.IFcm.MY_APPROVAL).observe(this, res -> {
                noOfReq = res.size();
                total = noOfChat + noOfReq;
                setCount();
            });

            messenger.getChatDBOns(false).observe(this, res -> {
                noOfChat = res.size();
                total = noOfChat + noOfReq;
                setCount();
            });
        }
    }

    private void setCount() {
       /* if (search != null) {
            if (total > 0) {
               // search.setVisibility(View.VISIBLE);
            } else {
                search.setVisibility(View.GONE);
            }
          //  search.setText(total + "");
        }*/
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = MainActivity.this;
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewpager1);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        search = (ImageView) findViewById(R.id.search);
        more = (ImageView) findViewById(R.id.more);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabedit = (FloatingActionButton) findViewById(R.id.fabedit);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Setting.class));
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SeachhActivity.class));

            }
        });
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), context);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.getCurrentItem();
        tabLayout.setupWithViewPager(viewPager);



      /*  if(!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)){
            fab.setVisibility(View.GONE);
            fabedit.setVisibility(View.GONE);
            fab.setImageResource(R.drawable.ic_chat_black_24dp);}
        else {
            fab.setVisibility(View.VISIBLE);
            fabedit.setVisibility(View.GONE);
            fab.setImageResource(R.drawable.ic_chat_black_24dp);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(context, SeachhActivity.class));}
            });}
        if (tabLayout.getSelectedTabPosition() == 1) {
            if (!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)) {
                fab.setVisibility(View.GONE);
            } else {
                fab.setVisibility(View.VISIBLE);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(context, SeachhActivity.class));
                    }
                });
            }}
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        fabedit.setVisibility(View.GONE);
                        fab.setVisibility(View.GONE);
                        break;

                    case 1:

                        android.support.v4.app.Fragment f = getSupportFragmentManager().findFragmentById(R.id.activity_login);
                        if (f != null && (f instanceof NewUserFragment)) {                            // do something with f
                            Toast.makeText(context, "new", Toast.LENGTH_SHORT).show();
                            fab.setVisibility(View.GONE);
                            fabedit.setVisibility(View.GONE);
                            fab.setImageResource(R.drawable.ic_emoji_people_light_activated);
                        } else {
                            fab.setVisibility(View.VISIBLE);
                            fabedit.setVisibility(View.GONE);
                            fab.setImageResource(R.drawable.ic_chat_black_24dp);
                            fab.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                  //  startActivity(new Intent(context, SelectConatct_Activity.class));
                                }
                            });
                        }
                        break;
                    case 2:
                        fabedit.setVisibility(View.GONE);
                        fab.setImageResource(R.drawable.ic_call_black_24dp);
                        fab.setVisibility(View.VISIBLE);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                               // startActivity(new Intent(context,SelectConatct_Activity.class));
                            }

                        });
                        break;
                    case 3:
                        fabedit.setVisibility(View.GONE);
                        fab.setImageResource(R.drawable.ic_call_black_24dp);
                        fab.setVisibility(View.VISIBLE);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {}
                        });
                        break;
                    // do something here
                }
                super.onTabSelected(tab);
            }
        });*/
        setObserver();
       // setupTabIcons();
    }
         private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);}
    ContainerChatFragment.INavigation listner;
    public void setListener(ContainerChatFragment.INavigation listner){
        this.listner=listner;
    }
    public interface INavigation2{
        void navigateToChat();
    }@Override
    protected void onRestart() {
        super.onRestart();
      //  Toast.makeText(context, "restart", Toast.LENGTH_SHORT).show();


       /* if (!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)) {
            //  Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
            fab.setVisibility(View.GONE);


        } else {
            fab.setVisibility(View.VISIBLE);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(context, SeachhActivity.class));
                }
            });
            //  Toast.makeText(context, "true", Toast.LENGTH_SHORT).show();
        }*/


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}




