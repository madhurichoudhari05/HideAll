package com.example.admin.a100dark.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Activity.AccountActivity;
import com.example.admin.a100dark.Activity.Chats;
import com.example.admin.a100dark.Activity.Contacts;
import com.example.admin.a100dark.Activity.DataUsage;
import com.example.admin.a100dark.Activity.Help;
import com.example.admin.a100dark.Activity.Notitication;
import com.example.admin.a100dark.Activity.ProfileActivty;
import com.example.admin.a100dark.Activity.Setting;
import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

/**
 * Created by Rajesh on 2/2/2018.
 */

public class FragmentSetting extends Fragment {
    Context mContext;
    LinearLayout linearAccount, linearLayoutChat, linearLayoutNotication,linearLayoutDatauseg, linearLayoutContacts, linearLayouthelp;
    LinearLayout arrowmessenger,profilelayout;
    ImageView back,toolbar_back;
    TextView toolTitle;
    private TextView userName,status_time;





    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.setting, null);
        mContext=getActivity();

        linearAccount = (LinearLayout)vv.findViewById(R.id.linearAccount);
        // arrowmessenger = (LinearLayout) findViewById(R.id.arrowmessenger);
        linearLayoutChat = (LinearLayout)vv.findViewById(R.id.linearChat);
        linearLayoutNotication = (LinearLayout)vv.findViewById(R.id.linear_layoutNotication);
        linearLayoutDatauseg = (LinearLayout)vv.findViewById(R.id.linear_layoutDatausage);
        linearLayoutContacts = (LinearLayout)vv.findViewById(R.id.linear_layoutConatcts);
        linearLayouthelp = (LinearLayout)vv.findViewById(R.id.linear_layoutHelp);
        profilelayout = (LinearLayout)vv.findViewById(R.id.profilelayout);
        back= (ImageView)vv. findViewById(R.id.toolbar_back);
        toolTitle= (TextView)vv. findViewById(R.id.toolbar_title);
        userName= (TextView)vv. findViewById(R.id.status_user_name);
        status_time= (TextView)vv. findViewById(R.id.status_time);
        userName.setText(CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_NAME));
        status_time.setText(CommonUtils.getPreferencesString(getActivity(), AppConstants.Registerd_MOBILE_NO));
        toolTitle.setText("Setting");


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().finish();

            }
        });
        profilelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,ProfileActivty.class));


            }
        });
        linearLayoutChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,Chats.class));

            }
        });
        linearLayoutNotication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,Notitication.class));

            }
        });


        linearLayoutDatauseg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,DataUsage.class));

            }
        });



        linearLayoutContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,Contacts.class));

            }
        });
        linearLayouthelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,Help.class));

            }
        });
        linearAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, AccountActivity.class));

            }
        });

        return vv;
    }

}
