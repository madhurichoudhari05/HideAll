package com.example.admin.a100dark.Fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.MainActivity;
import com.example.admin.a100dark.Activity.SeachhActivity;
import com.example.admin.a100dark.Activity.VerificationActivity;
import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.ChatModel;
import com.example.admin.a100dark.Model.AllUserListModel;
import com.example.admin.a100dark.Model.CurrentUserDetails;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.chat.app.ui.FcmMessengerViewModel;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

/**
 * Created by Admin on 10/31/2017.
 */

public class ChatFragment extends Fragment {

    ArrayList<ChatModel> arrayList = new ArrayList<>();
    List<AllUserListDetailsModel> userList = new ArrayList<>();
    List<UserStatusDto> userListDb = new ArrayList<>();
    RecyclerView recyclerView;
    FloatingActionButton fab,fabParent;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private Context context;
    private MainActivity mActivity;
    private int user_id = 0;
    private ChatAdapter chatAdapter;
    private AppDatabase mAppDb;
    private FcmMessengerViewModel fcmVM;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.chat_screen, null);
        context = getActivity();
        //CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,true);
        user_id = CommonUtils.getIntPreferences(context, AppConstants.USER_ID);




        mActivity = (MainActivity) getActivity();
        setObserver();
        mAppDb = MyApplication.getDb();

        recyclerView = (RecyclerView) vv.findViewById(R.id.chat_recyclerview);
        fab = (FloatingActionButton) vv.findViewById(R.id.fab);
        fabParent = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, SeachhActivity.class));
                /*FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.frame, new Sel());
                fragmentTransaction.commit();*/
            }
        });

        chatAdapter = new ChatAdapter(userListDb, getActivity(), mActivity, fcmVM);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(chatAdapter);
      //  callUserListApi();
       /* if(user_id==0){

            CommonUtils.snackBar("User id null",recyclerView);
        }

        else {
            callUserListApi();
        }*/

        return vv;
    }

    private void callUserListApi() {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<List<AllUserListModel>> call = service.getUserListApi(user_id);
        call.enqueue(new Callback<List<AllUserListModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<AllUserListModel>> call, retrofit2.Response<List<AllUserListModel>> response) {
                List<AllUserListModel> userListModelList = response.body();

                userList.clear();

                try {

                    if (userListModelList.get(0).getCurrentUser().getUserName() != null) {
                        CommonUtils.savePreferencesString(context, AppConstants.USER_NAME, userListModelList.get(0).getCurrentUser().getUserName());

                    }
                }catch (NullPointerException e) {
                    e.printStackTrace();
                }

                List<UserStatusDto> users = new ArrayList<>();
                if (userListModelList != null && userListModelList.size() > 0) {
                    for (int i = 0; i < userListModelList.size(); i++) {
                        userList.addAll(userListModelList.get(i).getAllUser());

                        for (AllUserListDetailsModel model : userListModelList.get(i).getAllUser()) {

                            users.add(new UserStatusDto(
                                    model.getId() + "",
                                    model.getUserName() + "",
                                    model.getDeviceToken() + "",
                                    true,
                                    true,
                                    model.getUserPic() + "",
                                    ""
                            ));

                        }
                    }

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
//                                mAppDb.userDao().nukeTable();
                            mAppDb.userDao().insertAll(users);
                        }
                    }).start();

                    chatAdapter.notifyDataSetChanged();

                } else {
                    CommonUtils.snackBar("Not Fetched", recyclerView);
                }

                //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<List<AllUserListModel>> call, Throwable t) {
            }
        });
    }


    private void setObserver() {
        if (fcmVM == null) {
            fcmVM = ViewModelProviders.of(this).get(FcmMessengerViewModel.class);

            /*1 : Req user*/
            fcmVM.getUserListObs().observe(this, res -> {


                userListDb.addAll(res);
//                Log.e("size",res.size()+"");
            });
        }
    }
}
