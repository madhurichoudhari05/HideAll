package com.example.admin.a100dark.chat.app.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


/**
 * Created by abul on 23/11/17.
 */

@Entity(tableName = "user_table")
public class UserStatusDto {

    @PrimaryKey
    @NonNull
    private String toId;
    private String toName;
    private String fcmKey;
    private boolean requestStatus;
    private boolean approveStatus;
    private String toImage;
    private String latestMsg;

    public UserStatusDto() {
    }

    @Ignore
    public UserStatusDto(@NonNull String toId, String fcmKey, boolean approveStatus,boolean requestStatus,String toName) {
        this.toId = toId;
        this.fcmKey = fcmKey;
        this.approveStatus = approveStatus;
        this.requestStatus=requestStatus;
        this.toName =toName;
    }

    /*With image*/
    @Ignore
    public UserStatusDto(@NonNull String toId, String fcmKey, boolean approveStatus,boolean requestStatus,String toName,String toImage) {
        this.toId = toId;
        this.fcmKey = fcmKey;
        this.approveStatus = approveStatus;
        this.requestStatus=requestStatus;
        this.toName =toName;
        this.toImage =toImage;
    }

    /*with image nad latest msg*/

    public UserStatusDto(@NonNull String toId, String toName, String fcmKey, boolean requestStatus, boolean approveStatus, String toImage, String latestMsg) {
        this.toId = toId;
        this.toName = toName;
        this.fcmKey = fcmKey;
        this.requestStatus = requestStatus;
        this.approveStatus = approveStatus;
        this.toImage = toImage;
        this.latestMsg = latestMsg;
    }

    public String getLatestMsg() {
        return latestMsg;
    }

    public void setLatestMsg(String latestMsg) {
        this.latestMsg = latestMsg;
    }

    public String getToImage() {
        return toImage;
    }

    public void setToImage(String toImage) {
        this.toImage = toImage;
    }

    @NonNull
    public String getToId() {
        return toId;
    }

    public void setToId(@NonNull String toId) {
        this.toId = toId;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }

    public boolean isApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(boolean approveStatus) {
        this.approveStatus = approveStatus;
    }

    public boolean isRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(boolean requestStatus) {
        this.requestStatus = requestStatus;
    }
}