package com.example.admin.a100dark.chat.utils;


import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;

/**
 * Created by abul on 14/1/18.
 */

public class UserMsgDetail {

    private UserStatusDto details;
    private int count;


    public UserMsgDetail() {
    }

    public UserMsgDetail(UserStatusDto details, int count) {
        this.details = details;
        this.count = count;
    }

    public UserStatusDto getDetails() {
        return details;
    }

    public void setDetails(UserStatusDto details) {
        this.details = details;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
