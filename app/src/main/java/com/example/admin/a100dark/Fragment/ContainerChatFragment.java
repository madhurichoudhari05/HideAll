package com.example.admin.a100dark.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.a100dark.Adapter.ChatViewPagerAdapter;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.interfaces.OnBackpressedInterface;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

/**
 * Created by Admin on 10/31/2017.
 */

public class ContainerChatFragment extends Fragment {
    private Context mcontext;
    ChatViewPagerAdapter viewPagerAdapter;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
//    ViewPager viewPager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.conatiner_chat, container, false);

        mcontext = getActivity();
//        viewPager=vv.findViewById(R.id.llConatiner);

        return vv;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


       if(!CommonUtils.getPreferencesBoolean(mcontext, AppConstants.FIRST_TIME_LOGIN)){

            FragmentManager fragmentManager=getChildFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            NewUserFragment frag=new NewUserFragment();
            frag.setListener(this::navigateToChat);
            transaction.replace(R.id.activity_login, frag);
            transaction.commit();
        }
        else {
           FragmentManager fragmentManager=getChildFragmentManager();
           FragmentTransaction transaction = fragmentManager.beginTransaction();
           ChatFragment frag=new ChatFragment();
           transaction.replace(R.id.activity_login, frag);
           transaction.commit();
        }
    }

    public void navigateToChat(){

        CommonUtils.savePreferencesBoolean(mcontext,AppConstants.FIRST_TIME_LOGIN,true);

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_login, new ChatFragment());
        transaction.commit();
    }



    /*public boolean onBackPressed() {
        // currently visible tab Fragment
        OnBackpressedInterface currentFragment = (OnBackpressedInterface) viewPagerAdapter.getRegisteredFragment(viewPager.getCurrentItem());

        if (currentFragment != null) {
            // lets see if the currentFragment or any of its childFragment can handle onBackPressed
            return currentFragment.onBackPressed();
        }

        // this Fragment couldn't handle the onBackPressed call
        return false;
    }*/

    public interface INavigation{
        void navigateToChat();
    }
}
