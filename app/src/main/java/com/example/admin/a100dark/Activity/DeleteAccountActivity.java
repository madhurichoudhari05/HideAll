package com.example.admin.a100dark.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.StateAdapterDialog;
import com.example.admin.a100dark.Model.CountryCodeDetails;
import com.example.admin.a100dark.Model.CountryCodeResponse;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.interfaces.StateInterface;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.HintSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteAccountActivity extends AppCompatActivity implements StateInterface {


    private HintSpinner spinner;
    private Context context;
    private ArrayAdapter countyAdapter;
    private static final int TYPE_STATE = 0, TYPE_CITY = 1;

    private List<String> countryList1 = new ArrayList<>();
    private List<String> countryCodeList = new ArrayList<>();
    private Dialog dialog;
    private String stateName = "", state_id = "";
    private TextView tvSubmit, tvCountryName;
    EditText tvCountryCode;
    private ImageView back_number5;

    Button deleteButton;
    EditText delete_no_edit;
    String number;
    FileUploadInterface service;
    String currentNo;
    CardView changeNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_account);
        context = DeleteAccountActivity.this;
        spinner = (HintSpinner) findViewById(R.id.spinner);

        tvCountryCode = (EditText) findViewById(R.id.tvCountryCode);
        tvCountryName = (TextView) findViewById(R.id.tvCountryName);
        delete_no_edit = (EditText) findViewById(R.id.delete_no_edit);
        changeNumber = (CardView) findViewById(R.id.changeNumber);
        back_number5 = (ImageView) findViewById(R.id.back_number5);

        currentNo = CommonUtils.getPreferencesString(context, AppConstants.Registerd_MOBILE_NO);

        countyAdapter = new ArrayAdapter(context, R.layout.spinner_layout, R.id.tvspinner, countryList1);
        countyAdapter.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(countyAdapter);

        service = RetrofitHandler.getInstance().getApi();


        back_number5.setOnClickListener(v -> {
           finish();
        });

        tvCountryName.setOnClickListener(v -> {
            calCountryCode("id");
        });

        deleteButton = findViewById(R.id.deleteButton);

        deleteButton.setOnClickListener(v -> {

                    number = delete_no_edit.getText().toString();
                    if (number.length() == 0 || number.length() < 10 || !number.equals(currentNo)) {
                        Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
                    } else {
                        callDeleteApi(number);
                    }
                }
        );


    }

    private void callDeleteApi(String number) {

        Call<ResponseBody> bodyCall = service.deleteUser(number);

        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String res = response.body().string();
                    JSONArray jsonArray = new JSONArray(res);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("msg");
                    if (status == 1) {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(DeleteAccountActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }


    private void calCountryCode(final String user_id) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<CountryCodeResponse> call = service.getCountry_Code(user_id);
        call.enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(retrofit2.Call<CountryCodeResponse> call, retrofit2.Response<CountryCodeResponse> response) {
                CountryCodeResponse CountryCodeResponse = response.body();
                countryList1.clear();
                countryCodeList.clear();
                if (CountryCodeResponse != null && CountryCodeResponse.getData().size() > 0) {
                    List<String> countryCodeList = new ArrayList<>();
                    //  List<String> countryCodeList=new ArrayList<>();
                  /*  for (int j= 0; j <CountryCodeResponse.getData().size(); j++) {
                        countryList1.add(CountryCodeResponse.getData().get(j).getPhonecode()+" ("+(CountryCodeResponse.getData().get(j).getName()+")"));
                      // countryList1.add(CountryCodeResponse.getData().get(j).getSortname());
                        countryCodeList.add(CountryCodeResponse.getData());
                    }*/
                    callLocationSateDialog(CountryCodeResponse.getData());

                    /*countyAdapter = new ArrayAdapter(context, R.layout.spinner_layout, R.id.tvspinner, countryList1);
                    countyAdapter.setDropDownViewResource(R.layout.spinner_layout);
                    spinner.setAdapter(countyAdapter);*/
                    //   CommonUtils.snackBar("saved", phone_login);
                } else {
                    CommonUtils.snackBar(" Not Fetched", spinner);
                }
                //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<CountryCodeResponse> call, Throwable t) {

            }
        });
    }


    private void callLocationSateDialog(List<CountryCodeDetails> list) {
        dialog = new Dialog(context, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_occupation);
       // dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        ImageView leftarrow = (ImageView) dialog.findViewById(R.id.leftarrow);
        EditText editMobileNo = (EditText) dialog.findViewById(R.id.etSearch);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        final ListView lv = (ListView) dialog.findViewById(R.id.listview);
        TextView saveImage = (TextView) dialog.findViewById(R.id.saveImage);

        //  CustomListAdapterDialogbloodgroup clad = new CustomListAdapterDialogbloodgroup(getActivity(), stateList1);
        StateAdapterDialog stateAdapterDialog = new StateAdapterDialog((Activity) context, list, this, TYPE_STATE);
        lv.setAdapter(stateAdapterDialog);
        if (state_id != null) {
            stateAdapterDialog.setSelection(state_id);
        }


        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        editMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                stateAdapterDialog.getFilter().filter(arg0);
                stateAdapterDialog.getViewTypeCount();

              /*  if(arg1==1) {
                    stateAdapterDialog.getFilter().filter(arg0);
                    stateAdapterDialog.getViewTypeCount();
                }*/
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        dialog.show();
    }

    @Override
    public void setValue(String name, String id, boolean seletion) {

        stateName = name;
        state_id = id;

        CommonUtils.savePreferencesString(context, AppConstants.COUNTRY_CODE, state_id);

        if (seletion) {


            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Thread.sleep(500);
                        dialog.dismiss();

                    } catch (InterruptedException e) {

                        e.printStackTrace();

                    }

                }
            }).start();

            tvCountryCode.setText("+ " + state_id);
            tvCountryName.setText(stateName);

        }


    }

    @Override
    public void setValue(List<CountryCodeDetails> currentSelected, int type) {

    }
}
