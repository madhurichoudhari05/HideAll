package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

/**
 * Created by Admin on 11/2/2017.
 */


public class Splash_screens extends AppCompatActivity {

   private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreens);
        context=Splash_screens.this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2500);}
                catch (InterruptedException e) {

                    e.printStackTrace();}

             /*   Intent i = new Intent(Splash_screens.this, LoginActivity.class);
                startActivity(i);
                finish();*/

                if (!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)) {
                    Intent mainIntent = new Intent(Splash_screens.this, LoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent mainIntent = new Intent(Splash_screens.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                }}
        }).start();
    }

    }
/*   AIzaSyC_QW0rp1O2aWp32ee4g-dLECPyiJwMAbk
AIzaSyC_QW0rp1O2aWp32ee4g-dLECPyiJwMAbk
*
* */