package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.admin.a100dark.R;

/**
 * Created by Admin on 11/9/2017.
 */

public class AccountActivity extends AppCompatActivity {

    LinearLayout arrowmessenger;
    LinearLayout changeNoLinear;
    private View deleteLinear;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accountactivyty);
        arrowmessenger = (LinearLayout) findViewById(R.id.arrowmessenger);
        changeNoLinear = (LinearLayout) findViewById(R.id.changeNoLinear);
        deleteLinear = (LinearLayout) findViewById(R.id.deleteLinear);

        Log.e("ohk","fine");


        arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();

            }
        });

        changeNoLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this, ChangeNumberActivity1.class));

            }
        });
        deleteLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this, DeleteAccountActivity.class));

            }
        });

    }
}
