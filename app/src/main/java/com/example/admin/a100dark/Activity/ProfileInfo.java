package com.example.admin.a100dark.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Fragment.NewUserFragment;
import com.example.admin.a100dark.R;

import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.CropActivity;
import com.example.admin.a100dark.utils.GlobalAccess;
import com.example.admin.a100dark.utils.MadhuFileUploader;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Admin on 11/8/2017.
 */

public class ProfileInfo extends CommonBaseActivity {


    public static final String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    public static final String EXCEPTION_MSG_TIMEOUT = "Poor network connection"/*"Please try again later."*/;
    TextView submitmain;
    private EditText etName;
    int TAKE_PHOTO_CODE = 0;
    ImageView cameraopen,profile_image;
    private boolean isFileImg;
    public static int count = 0;
    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String mCurrentPhotoPath = "";
    private ArrayList<String> imagelist;
    private Context mcontext;
    private Activity mActivity;
 //   private String USER_ID="";
    private int user_id=0;

    private   View rootviewProfile;
    private Dialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profileinfo);
        imagelist = new ArrayList<>();
        mcontext=ProfileInfo.this;
        mActivity=ProfileInfo.this;
        user_id=CommonUtils.getIntPreferences(mcontext, AppConstants.USER_ID);
        submitmain = (TextView) findViewById(R.id.submitmain);
        etName = (EditText) findViewById(R.id.etName);
        rootviewProfile = findViewById(R.id.rootviewProfile);
        cameraopen = (ImageView) findViewById(R.id.cameraopen);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        //  CommonUtils.saveIntPreferences(context, AppConstants.USER_ID,registerList.get(0).getUserId());
        cameraopen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCameraGalleryDialog();
            }
        });

        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCameraGalleryDialog();
            }
        });


        submitmain.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if(doValidation()){
                    callProfileData();
                }
            }
        });

    }private boolean doValidation() {

        if(TextUtils.isEmpty(etName.getText().toString())){
            CommonUtils.snackBar("Please Enter Your Name.", etName);
            return false;
        }
        else {

            return true;
        }
    }


    public void openCameraGalleryDialog() {
        final CharSequence[] items = {"Open Camera", "Open Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(
                this);
        builder.setTitle("Select : ");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera")) {
                    cameraPermissionMethod();
                } else if (items[item].equals("Open Gallery")) {
                    gallleryPermissionMethod();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }private void cameraPermissionMethod() {
        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            activityForCamera();
        }}private void gallleryPermissionMethod() {
        if (requestPermission(GALLERY_REQUEST_CODE, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE})) {
            activityForGallery();
        }
    }
    //  }


    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry, There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(mActivity, getApplicationContext()
                                .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList = getPackageManager()
                                    .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }


    private void activityForGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select : "),
                REQUEST_GALLERY);
    }


    private void cameraOperation(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            GlobalAccess.setImagePath(imageUrl);
            Intent i = new Intent(mActivity, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }

    private void galleryOperation(Intent data) {
        Uri selectedImage = data.getData();
        String picturePath = null;
        if (selectedImage == null) {
            Toast.makeText(mActivity, "Image selection Failed!", Toast.LENGTH_SHORT).show();
        } else {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                if (picturePath == null)
                    picturePath = selectedImage.getPath();
                cursor.close();
            } else {
                picturePath = selectedImage.getPath();
            }
        }
        if (picturePath != null) {
            GlobalAccess.setImagePath(picturePath);
            Intent i = new Intent(mActivity, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }

    @Override
    protected void onPermissionResult(int requestCode, boolean isPermissionGranted) {


            if (isPermissionGranted) {
                if (requestCode == CAMERA_REQUEST_CODE) {
                    activityForCamera();
                } else if (requestCode == GALLERY_REQUEST_CODE) {
                    activityForGallery();
                }
            }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    String imageFile = mCurrentPhotoPath;
                    cameraOperation(imageFile);
                    break;
                case REQUEST_GALLERY:
                    galleryOperation(data);
                    break;
                case CROP_REQUEST_CODE:
                    if (data.getExtras().containsKey("path")) {
                        imagelist.clear();
                        mCurrentPhotoPath = data.getExtras().getString("path");
                        imagelist.add(mCurrentPhotoPath);
                        Log.e("mCurrentPhotoPath", mCurrentPhotoPath);
                        Log.e("numerofimages", imagelist.toString());
                        Log.e("sizenumerofimages", String.valueOf(imagelist.size()));
                        isFileImg = true;
                        //  setProfileData();



                        CommonUtils.savePreferencesString(mcontext,AppConstants.SENDER_PROFILE_PIC,mCurrentPhotoPath);
                        Picasso.with(mActivity)
                                .load("file://" + mCurrentPhotoPath)
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.ic_launcher)
                                .into(profile_image);

                    } else {
                        mCurrentPhotoPath = null;
                    }
                    break;
            }
        }
    }
    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void callProfileData() {
        CommonUtils.showProgress(mcontext);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<ResponseBody> call=null;
        if (imagelist != null && imagelist.size() > 0) {


            Map<String, String> params = new HashMap<String, String>();
            params.put("user_id", String.valueOf(user_id));
            params.put("status","busy");
            params.put("name", etName.getText().toString());


            MadhuFileUploader uploader = new MadhuFileUploader();
            uploader.uploadFile(params, imagelist, "fileToUpload", mcontext);
            Log.e("retoImageupload", params.toString());
            for (String s : imagelist) {
                Log.e("imagelstttttt", s);
            }

           call = service.uploadFileProfileile(uploader.getPartMap(), uploader.getPartBodyArr());
           }
           else {

            imagelist.clear();
            RequestBody user_id1 = RequestBody.create(MediaType.parse("text/plain"),String.valueOf(user_id));
            RequestBody emoji_staus = RequestBody.create(MediaType.parse("text/plain"),"busy");
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), etName.getText().toString());
            call = service.uploadFileWithPartMap(user_id1, emoji_staus,name);}
            call.enqueue(new Callback<ResponseBody>() {
            public boolean status;

            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
            CommonUtils.dismissProgress();
                String str = "";

                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("retoImageupload", str);

                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        boolean profilepic = jsonObject.getBoolean("status");
                        String messageimg = jsonObject.getString("message");
                        CommonUtils.savePreferencesString(mcontext,AppConstants.USER_NAME,etName.getText().toString());
                       // CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,true);
                        if (profilepic) {
                          //  CommonUtils.snackBar(messageimg,cameraopen);
                            if (messageimg != null && !messageimg.isEmpty()) {
                                callcongratulation();
                            }
                                else {
                                Log.e("userpic", "null");}
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                CommonUtils.dismissProgress();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = EXCEPTION_MSG_TIMEOUT;
                } else if (t instanceof IOException) {
                    msg = EXCEPTION_MSG_TIMEOUT;

                } else if (t instanceof HttpException) {
                    msg = EXCEPTION_MSG;
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }



    private void callcongratulation() {

        dialog = new Dialog(mcontext, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_congratu);
        // dialog.getWindow().getAttributes().windowAnimations = animationdialog;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView tvOK = (TextView) dialog.findViewById(R.id.tvOK);

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);


        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent mainIntent = new Intent(ProfileInfo.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });

        dialog.show();



}}
