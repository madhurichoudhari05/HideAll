package com.example.admin.a100dark.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.a100dark.Model.CallModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.interfaces.CallInteface;

import java.util.ArrayList;

/**
 * Created by Admin on 11/1/2017.
 */

public class FirstCallFragment extends Fragment implements CallInteface {


    private Context mcontext;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.first_call_screen, null);
        mcontext=getActivity();



        return vv;
    }

    @Override
    public void call() {


    }
}
