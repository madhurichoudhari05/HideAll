package com.example.admin.a100dark.chat.app;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.mediaSelection.LocalFileUncaughtExceptionHandler;
import com.google.firebase.FirebaseApp;


/**
 * Created by abul on 11/9/2017.
 */

public class MyApplication extends MultiDexApplication implements IConstants.INetwork,IConstants {

    private static MyApplication societyApplication;
    public static Context mAppContext;
    private static int maxThread=4;
    private static AppDatabase db;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        Thread.setDefaultUncaughtExceptionHandler(new LocalFileUncaughtExceptionHandler(this,
                Thread.getDefaultUncaughtExceptionHandler()));

//        AndroidThreeTen.init(this);
        societyApplication = this;
        mAppContext = getApplicationContext();

        db = Room.databaseBuilder(this,
                AppDatabase.class, IDb.USER_DB)
                //DBMadhu
                .fallbackToDestructiveMigration()
                .build();



    }

    public static AppDatabase getDb(){
        return db;
    }

    public static synchronized MyApplication getInstance() {
        return societyApplication;
    }

    public static synchronized Context getmAppContext() {
        return mAppContext;
    }


}
