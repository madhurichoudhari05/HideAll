package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.NewBroadCastAdapter;
import com.example.admin.a100dark.Adapter.NewGroupAdapter;
import com.example.admin.a100dark.Model.NewBroadCast_model;
import com.example.admin.a100dark.R;

import java.util.ArrayList;

/**
 * Created by Admin on 11/10/2017.
 */

public class NewBRoasdCast extends AppCompatActivity {

    ImageView arrowmessenger ;
    RecyclerView recyclerViewnewbroadcast;
    Context context;
    ArrayList<NewBroadCast_model> arrayList= new ArrayList<>();
    NewBroadCastAdapter newBroadCastAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=NewBRoasdCast.this;
        setContentView(R.layout.newbroadcast);
        recyclerViewnewbroadcast = (RecyclerView)findViewById(R.id.call_recyclerviewnewbroadcast);

        arrowmessenger = (ImageView)findViewById(R.id.arrowmessenger);



/*
        recyclerViewnewbroadcast.setFocusable(false);

        recyclerViewnewbroadcast.setNestedScrollingEnabled(false);*/

        Toast.makeText(context, "Checked", Toast.LENGTH_SHORT).show();

        init();
        newBroadCastAdapter = new NewBroadCastAdapter(arrayList,context);
        recyclerViewnewbroadcast.setLayoutManager(new LinearLayoutManager(context));
        recyclerViewnewbroadcast.setAdapter(newBroadCastAdapter);
    }

    public void init()

    {
        for (int i = 0 ; i<10;i++) {
            NewBroadCast_model newCroadCast_model = new NewBroadCast_model("User name ","Today 9:00 a.m.",R.drawable.images);
            arrayList.add(newCroadCast_model);

        }
        arrowmessenger.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewBRoasdCast.this,MainActivity.class));

            }
        });
    }

}
