package com.example.admin.a100dark.chat.app.retofit;





import com.example.admin.a100dark.chat.app.retofit.model.fcmRes.FcmResponseDto;
import com.example.admin.a100dark.chat.app.retofit.model.notif.DataNotifDto;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

/**
 * Created by abul on 14/11/17.
 */

public interface FcmApi {

    /*@POST("/fcm/send")
    Observable<ResponseBody> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body Message message);*/

    @POST("/fcm/send")
    Observable<FcmResponseDto> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body DataNotifDto message);
}
