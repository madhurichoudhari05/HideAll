package com.example.admin.a100dark.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.admin.a100dark.Adapter.StateAdapterDialog;
import com.example.admin.a100dark.Model.Counterycode_Model;
import com.example.admin.a100dark.Model.CountryCodeDetails;
import com.example.admin.a100dark.Model.CountryCodeResponse;
import com.example.admin.a100dark.Model.Register_model;
import com.example.admin.a100dark.Model.RegisterrationModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.interfaces.StateInterface;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.HintSpinner;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import retrofit2.Callback;

import static com.example.admin.a100dark.R.style.animationdialog;


public class LoginActivity extends AppCompatActivity implements StateInterface {

    ImageView imageView, ivSpinnner;

    private Context context;

    public static int count = 0;

    HintSpinner spinner;

    String USER_ID = "user_id";

    Register_model register_modell;

    EditText phone_login;
    private String stateName = "", state_id = "";

    // EditText phone_login;

    private List<Counterycode_Model> countryListDeails = new ArrayList<>();
    private List<String> countryList1 = new ArrayList<>();
    private List<String> countryCodeList = new ArrayList<>();
    private ArrayAdapter countyAdapter;
    private String fireBaseKey = "";
    private String country_code = "", country_code1 = "";
    private TextView tvSubmit, tvCountryCode, tvCountryName,tvCoutydash;
    private static final int TYPE_STATE = 0, TYPE_CITY = 1;
    private Dialog dialog;
    private CheckBox checktem;
    private String confirmNumber="";
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    String mVerificationId;

    private String term="PLEASE READ THE FOLLOWING TERMS OF SERVICE AND LICENSE AGREEMENT CAREFULLY BEFORE DOWNLOADING, USING, OR ACCESSING ANY PART OF THE PRANKDIAL MOBILE APPLICATION OR THE PRANKDIAL WEBSITE.\n" +
            "The following are the terms and conditions for use of " +
            "www.whosmechat.com\n"+
            " and its services (the \"Website\") and the WhosmeApp Mobile Application and its services (the \"Application\") (collectively the Website and the Application are referred to as the \"Services\"). The Services are owned and operated by Fastener international llc (\"Who’sme \" or \"us,\" or \"we\").\n" +
            "By downloading or using the Application, accessing the Services, continuing to access the Services, or submitting any information through the Services, and in consideration for the services Whosme provides to you, YOU (the terms \"you\" or \"yours\" includes the person or entity that is accessing the Services as well as any third-parties accessing the Services on your behalf) ARE STATING THAT YOU AGREE TO BE BOUND BY ALL OF THESE TERMS AND CONDITIONS GOVERNING THE USE OF THE SERVICES (the \"Terms\"). Use of the Services is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices, including the class action waiver, contained herein.\n" +
            "I. OVERVIEW AND DISCLAIMERS\n" +
            "The Whosme Website, Application, and Services allow users to initiate prank telephone calls to their friends with texting ,voices, And video call . The initiating user is a party to these prank telephone calls. The Services are provided for entertainment purposes only. While some of the Services may be available for free, you can also make purchases, or undertake certain activities, to increase the Services' functionality, like increasing the number of calls you can initiate or to activate other features. Who’sme does not guarantee refunds for any purchases.\n" +
            "You are solely responsible for the calls you initiate through the Services and ensuring that the calls&Text comply with the federal and state laws applicable in the states in which you and the call recipient are located and under governments countries laws . You are strictly prohibited from using the Services to defraud, cause harm, wrongly obtain anything of value, send indecent or obscene content, or to annoy, abuse, threaten, harass, or make repeated telephone calls to another person. You may not use the Services for any commercial purposes. You must be at least 18 to use the Services and you may not initiate a call through the Services to anyone under age 18.\n" +
            "You are strictly prohibited from using WHO’SME to call emergency lines, health care facilities, government agencies, employees or officials, law enforcement, schools, numbers assigned to radio common carrier services, or any service for which the called party is charged for the call & Text , unless the call is made with the prior express consent of the called party. You are prohibited from using WHO’SME to call & Text any recipient that is prohibited by law or regulation. Those using the Services for prohibited purposes may be subject to certain identification requirements and prosecution.\n" +
            "WHO’SME routinely records the telephone calls made through the Services. You represent and warrant that you consent to the recording of any telephone call which you initiate through the Services and that you comply with all state and federal laws applicable to the recording of the telephone& Texting conversations initiated through the Services, including laws applicable to the states in which you and the call recipient are located.\n" +
            "\n" +
            "WHO’SME may also publicly post, or allow users to publicly post, the recording from any telephone conversation initiated through the Services. You represent and warrant that the posting of a recorded telephone conversation you initiate through the Services will not violate any state or federal law applicable to the states in which you and the call recipient are located.\n" +
            "If you want to block your telephone number from receiving voice& Texting & calls from the Services, use the form available at service just block that user block chatting .\n" +
            "II. REGISTRATION, PAYMENT, AND INFORMATION SUBMISSION\n" +
            "Registration. In order to utilize some of the Services' features, you must register and create an account. In order to create an account, you must complete the registration process by providing  WHO’SME with complete and accurate information as prompted by the registration form, including contact information, a username, and password. You may also need to set up a pin number to use certain features & fingers .print You shall protect your password and pin number and take full responsibility for your own and third party activities that occur under your account. You agree to notify WHO’SME immediately of any unauthorized use of your account or any other breach of security. If you create an account on behalf of an entity, these Terms bind both you and the entity.\n" +
            "Refusal of Service.WHO’SME  reserves the right, with or without notice, to terminate the account of, or refuse service to, any persons that violate these Terms, violate any party's intellectual property rights, abuse other users of the Services, misuse the Services, or otherwise engage in inappropriate conduct, as determined by  WHO’SME in its sole discretion.\n" +
            "Payment. If you make any purchases through the Application or the Services, you agree to pay all applicable fees for those purchases. Unless otherwise stated, all fees are stated in U.S. Dollars.\n" +
            "Age Restriction. By using the Services, you represent and warrant that you are 18 years of age or older. You may not use the Services to place calls to any person who is under 18 years of age.\n" +
            "Data Importation. The Services may allow you to import contact information from a contact list or other data source created by other another program not affiliated with WHO’SME . If you choose to import data into WHO’SME from any other source, you represent that you have the authority to import that data and that your importation of that data does not violate the rights or terms of any third-parties.\n" +
            "III. MODIFICATIONS TO TERMS OF SERVICE AND OTHER POLICIES\n" +
            " WHO’SME reserves the right to change or modify any of the terms and conditions contained in these Terms or any policy governing the use of the Services, at any time, by posting the new terms on the Website or Application. The most current version of these Terms will be located on the Application and on the Website. You understand and agree that your access to or use of the Services is governed by the then-current Terms that are effective at the time of your access to or use of the Services. If we make material changes to these Terms, we will notify you by email or by updating the \"Updated\" date indicated at the top of this page with the date that revisions to these Terms were last made. You should revisit these Terms on a regular basis as revised versions will be binding upon you. Any such modification will be effective upon our posting of such new Terms. You understand and agree that your continued access to or use of the Services after the effective date of modifications to the Terms indicates your acceptance of the modifications.\n" +
            "IV. PROPRIETARY RIGHTS\n" +
            "Except as expressly provided for in the non-exclusive license contained in Section V herein,  WHO’SME expressly reserves all right, title, and interest in and to the Services and the Services' content, including any copyright, patent, or trademark or other intellectual property right, or federal or state right, pertaining thereto.\n" +
            "V. WHO’SME MOBILE APPLICATION LICENSE\n" +
            "License. In order to use some of the Services' features, you need to download the Application. Subject to, and in accordance with, these Terms, PrankDial grants to You, and You accept from WHO’SME, a limited, revocable, non-exclusive, and non-transferable license to use the Application and its services. WHO’SME reserves all rights not expressly granted to You hereunder. This license is automatically revoked if you violate any of these Terms.\n" +
            "contact terms \n" +
            "By this agreement agreed you gave to us allowed to access to your contact and send to them invitation by our application name without showing them we got the contact from you .\n" +
            "Payment Terms. WHO’SME may make all or some of the Application and the Services available for free (the \"Free Services\"). WHO’SME does not guarantee that any portion of the Services will always be available for free and reserves the right to begin charging for any portion of the Services at any time. The Free Services may have reduced functionality. For example, the Free Services may only allow users to place a limited number of telephone calls per day. WHO’SME may allow users to increase or enhance the functionality of the Application and Services by making certain purchases, including in-app purchases, or undertaking certain activities. Please review the specific terms for your applicable purchase. If you make a purchase through the Services, then you consent to WHO’SME storing your payment information. Prices are subject to change. In the event the  WHO’SME services are discontinued for any reason, any tokens or other purchases will terminate and no refund will be made.\n" +
            "Free Trials. WHO’SME may offer free trial offers from time to time. The failure to cancel your free trial during the stated period will result in you being charged for the associated purchases beginning at the end of the trial period. For more details, please review the specific terms of the free trial offer.\n" +
            "Refund Policy.WHO’SME  does not guarantee refunds for lack of usage or dissatisfaction. Any tokens, calls, or other purchases made through the Services are nonrefundable.\n" +
            "Personal Use. You may only use the Application and the Services for your own personal use. You may not use the Services for any commercial use.\n" +
            "Source Code. The grant of this license is not, and shall not be construed as, a grant of any right to You to use, receive or view, copies of source code, schematics, master copies, design materials or other information used by You in creating, developing or implementing the Application, including updates or modifications thereto.  Without limiting the foregoing, You shall have no rights to receive any source code for the Application and shall not reverse engineer, disassemble or decompile, or otherwise attempt to derive source code for the Application for any purpose.\n" +
            "Prohibitions. You must not do, or permit others to do, any of the following: (a) copy or modify the Application in any way, except as expressly permitted in these Terms; (b) remove or modify WHO’SME's copyright notices, trademark, logo, legend or other notice of ownership from any originals or copies of the Application or Services; (c) attempt to view, read, modify, reverse compile, reverse assemble, disassemble or print the Application's source code or object code or other runtime objects or files distributed with the Application; (d) otherwise reverse engineer, modify or copy the look and feel, functionality or user interface of any portion of the Application or Services; (e) rent, lease, distribute (or redistribute), provide or otherwise make available the Application or Services, in any form, to any third party (including in any service bureau or similar environment); (f) use the Application or Services to process the data of third parties; or (g) use, install, or make available the Application, in whole or in part, through a wide area network including but not limited to World Wide Web sites, intranets, or Application Service Providers (ASP). In addition, You will not violate or attempt to violate the security of WHO’SME' networks or servers, including (x) access data not intended for You or log into a server or account which You are not authorized to access; (y) attempt to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper written request and authorization; or (z) attempt to interfere with service to any user, host or network, including by means of submitting a virus, overloading, flooding, spamming, mail bombing or crashing\n" +
            "You are also prohibited from taking any actions in violation of the prohibitions and requirements set forth in Section XII below.\n" +
            "Infringement. If a third party claims that the Application or the Services infringes its patent, copyright or trade secret, or any similar intellectual property right, WHO’SME will defend You against that claim at WHO’SME’s expense and pay all damages that a court finally awards, provided that You promptly notify WHO’SME in writing of the claim, and You allow WHO’SME to control and You cooperate with WHO’SME in, the defense or any related settlement negotiations. If such a claim is made or appears possible, You agree to permit WHO’SME to modify the Application or Services in order to attempt to avoid such claims. If WHO’SME determines that this alternative is not reasonably available, You agree to uninstall or return the Application on the WHO’SME’s request, and without any further liability or obligation of PrankDial. Further, WHO’SME shall have no obligation whatsoever for any claim based on Your modification of the Application or Your combination, operation, or use with any product, data or apparatus not specified or provided by WHO’SME. THIS PARAGRAPH STATES PRANKDIAL'S ENTIRE OBLIGATION TO YOU WITH RESPECT TO ANY CLAIM OF INFRINGEMENT.\n" +
            "Maintenance or Support. WHO’SME is not under any obligation to provide maintenance or support for the Application. WHO’SME may provide maintenance or support for the Application in WHO’SME's sole discretion.\n" +
            "Uninstallation. You may, at any time, uninstall the Application by utilizing your mobile device's procedures for uninstalling downloaded applications. However, as provided in the PrankDial Privacy Policy, WHO’SME may retain collected data after the uninstallation.\n" +
            "Third-Party Terms. You must comply with applicable third-party terms of service when using the Application, including any terms required by your mobile carrier.\n" +
            "Apple iOS, Microsoft Windows, and Google-Specific Terms. The following additional terms and disclosures only apply to you if you use the Application through the Apple iOS operating system, the Microsoft Windows operating system, or the Google Android operating system, if available and as applicable:\n" +
            "iOS - Apple\n" +
            "\t1.\tThis is an agreement between you and WHO’SME, and not with Apple. Apple is not responsible for the Application or Services and the related content.\n" +
            "\t2.\tWHO’SME grants you the non-transferable right to use this Application only on an iOS product that you own or control and as permitted by the Usage Rules set forth in the App Store Terms of Service.\n" +
            "\t3.\tApple has no obligation whatsoever to furnish any maintenance and support services with respect to this Application.\n" +
            "\t4.\tIn the event of any failure of this Application to conform to any applicable warranty, you may notify Apple, and Apple will refund the purchase price, if applicable, for this Application to you; and to the maximum extent permitted by applicable law, Apple will have no other warranty obligation whatsoever with respect to this Application.\n" +
            "\t5.\tApple is not responsible for addressing any claims by you or any third party relating to this Application or your possession and/or use of this Application, including but not limited to: (a) product liability claims; (b) any claim the Application fails to conform to any applicable legal or regulatory requirement; and (c) claims arising under consumer protection or similar legislation.\n" +
            "\t6.\tApple is not responsible for the investigation, defense, settlement, and discharge of any third party intellectual property infringement claim.\n" +
            "\t7.\tYou represent and warrant that you are not located in a country that is subject to a U.S. Government embargo, or that has been designated by the U.S. Government as a \"terrorist supporting\" country, and that you are not listed on any U.S. Government list of prohibited or restricted parties.\n" +
            "\t8.\tFastener International LLC. is the developer of the WHO’SME Mobile Application and questions, complaints or claims with respect to the Application can be sent to Fastener International LLC\n" +
            "10720 Dallas Tx75238 US.\n" +
            "\t9.\tApple and Apple’s subsidiaries are third party beneficiaries of these Terms, and, upon your acceptance, Apple as a third party beneficiary thereof will have the right (and will be deemed to have accepted the right) to enforce these Terms against you.";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        mAuth=FirebaseAuth.getInstance();


        getToken();
        if(!CommonUtils.getPreferencesBoolean(context, AppConstants.CURRENT_LONGI)){
            callTermCondDailog();


        }
        else {

        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        imageView = (ImageView) findViewById(R.id.image);
        ivSpinnner = (ImageView) findViewById(R.id.ivSpinnner);
        phone_login = (EditText) findViewById(R.id.mob_no);
        // phone_login = (EditText) findViewById(R.id.login_phone);
        spinner = (HintSpinner) findViewById(R.id.spinner);
        tvSubmit = (TextView) findViewById(R.id.tvSubmit);
        tvCountryCode = (TextView) findViewById(R.id.tvCountryCode);
        tvCoutydash = (TextView) findViewById(R.id.tvCoutydash);

        tvCountryName = (TextView) findViewById(R.id.tvCountryName);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(phone_login.getWindowToken(), 0);
        phone_login.setFocusable(false);
        phone_login.setFocusableInTouchMode(true);
        getWindow().getDecorView().clearFocus();

        countyAdapter = new ArrayAdapter(context, R.layout.spinner_layout, R.id.tvspinner, countryList1);
        countyAdapter.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(countyAdapter);
        //  calCountryCode("id");








        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.d("TAG", "onVerificationCompleted:" + credential);

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w("TAG", "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Snackbar snackbar = Snackbar
                            .make((LinearLayout) findViewById(R.id.lllogin), "Verification Failed !! Invalied verification Code", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }
                else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar snackbar = Snackbar
                            .make((LinearLayout) findViewById(R.id.lllogin), "Verification Failed !! Too many request. Try after some time. ", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }

            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d("TAG", "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };

        tvCountryName.setOnClickListener(v -> {
            calCountryCode("id");
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkValidation();

            }
        });


        phone_login.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {


            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            public void afterTextChanged(Editable arg0) {


                /*if (arg0.length() >= 10) {
                    callConfirmDailog();
                }*/


                /*if (arg0.length()==9) {

                    CommonUtils.hide_keyboard((Activity) context);
                    CommonUtils.snackBar("Mobile Number Should Be  1 To 10 Digits..", spinner);
                }
                else*/ /*if (arg0.length() >11){
                    CommonUtils.hide_keyboard((Activity) context);
                    CommonUtils.snackBar("Mobile Number Should  Not Be  greater than 10 Digits..", spinner);*/



            }
        });

        ivSpinnner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinner.performClick();
            }
        });

    }



    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                           /* mVerified = true;
                            timer.cancel();
                            verifiedimg.setVisibility(View.VISIBLE);
                            timertext.setVisibility(View.INVISIBLE);
                            phoneed.setEnabled(false);
                            codeed.setVisibility(View.INVISIBLE);*/
                            Snackbar snackbar = Snackbar
                                    .make((LinearLayout) findViewById(R.id.lllogin), "Successfully Verified", Snackbar.LENGTH_LONG);

                            snackbar.show();
                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w("TAG", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Snackbar snackbar = Snackbar
                                        .make((LinearLayout) findViewById(R.id.lllogin), "Invalid OTP ! Please enter correct OTP", Snackbar.LENGTH_LONG);

                                snackbar.show();
                            }
                        }
                    }
                });
    }
    private void callTermCondDailog() {


        dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.terms_dialog);
       // dialog.getWindow().getAttributes().windowAnimations = animationdialog;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        checktem = (CheckBox) dialog.findViewById(R.id.checktem);
        TextView details = (TextView) dialog.findViewById(R.id.details);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        details.setText(term);



        checktem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    dialog.dismiss();
                }
                else {
                    CommonUtils.snackBar("Please accept  term & Condition.", spinner);
                }

            }
        });

        dialog.show();

    }



    private void callConfirmDailog() {


        dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirm_dialog);
        // dialog.getWindow().getAttributes().windowAnimations = animationdialog;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        checktem = (CheckBox) dialog.findViewById(R.id.checktem);
        TextView tvNumber = (TextView) dialog.findViewById(R.id.tvNumber);
        TextView tvCode = (TextView) dialog.findViewById(R.id.tvCountryCode);
        TextView tvEdit = (TextView) dialog.findViewById(R.id.tvEdit);
        TextView submitContinu = (TextView) dialog.findViewById(R.id.submitContinu);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        tvNumber.setText(phone_login.getText().toString());
        tvCode.setText(state_id+"  -  ");
        confirmNumber=(phone_login.getText().toString());



        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvNumber.requestFocusFromTouch();
                tvNumber.requestFocus();
                tvNumber.setFocusableInTouchMode(true);
                tvNumber.setCursorVisible(true);
                tvNumber.setCursorVisible(true);
                tvNumber.setFocusable(true);
                tvNumber.setEnabled(true);




            }
        });


        submitContinu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                CommonUtils.showProgress(context);
                calRegidterApi();


            }
        });



        dialog.show();

    }


    private void checkValidation() {


        if (tvCountryName.getText().toString().equalsIgnoreCase("Choose your Country")) {
            CommonUtils.snackBar("Please Select Country Name.", spinner);

        } else if (phone_login.getText().toString().length() < 10) {
            CommonUtils.snackBar("Mobile Number Should Be In between 1 To 10 Digits..", spinner);
        }

        else if (phone_login.getText().toString().length() >11) {
            CommonUtils.hide_keyboard((Activity) context);
            CommonUtils.snackBar("Mobile Number Should  Not Be  greater than 10 Digits..", spinner);
        }
        else {

            callConfirmDailog();

            //  LoginMethod();
        }


    }

    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(context, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {

            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);

            //Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();

            //  CommonUtils.snackBar();

            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(context, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    private void calCountryCode(final String user_id) {

        CommonUtils.showProgress(context);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<CountryCodeResponse> call = service.getCountry_Code(user_id);
        call.enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(retrofit2.Call<CountryCodeResponse> call, retrofit2.Response<CountryCodeResponse> response) {
                CommonUtils.dismissProgress();
                CountryCodeResponse CountryCodeResponse = response.body();
                countryList1.clear();
                countryCodeList.clear();
                if (CountryCodeResponse != null && CountryCodeResponse.getData().size() > 0) {
                    List<String> countryCodeList = new ArrayList<>();
                    callLocationSateDialog(CountryCodeResponse.getData());

                } else {
                    CommonUtils.snackBar(" Not Fetched", phone_login);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<CountryCodeResponse> call, Throwable t) {

            }
        });
    }
    private void calRegidterApi() {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<List<RegisterrationModel>> call = service.getRegister(FirebaseInstanceId.getInstance().getToken(), confirmNumber, state_id);
        call.enqueue(new Callback<List<RegisterrationModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<RegisterrationModel>> call, retrofit2.Response<List<RegisterrationModel>> response) {


              CommonUtils.dismissProgress();

                List<RegisterrationModel> registerList = response.body();
                //countryList1.clear();
               // countryCodeList.clear();
                if (registerList != null && registerList.size() > 0) {
                    if (registerList.get(0).getStatus()) {

                            if (registerList.get(0).getOtp() == null && "".equals(registerList.get(0).getOtp())) {

                                CommonUtils.snackBar("Otp is not send on this Mobile Number.", phone_login);

                               CommonUtils.snackBar(registerList.get(0).getMessage(), tvSubmit);
                        } else {
                                CommonUtils.savePreferencesString(context, AppConstants.Registerd_MOBILE_NO, phone_login.getText().toString());

                                CommonUtils.saveIntPreferences(context, AppConstants.USER_ID, registerList.get(0).getUserId());
                                CommonUtils.saveIntPreferences(context, AppConstants.SIGNUP_OTP, registerList.get(0).getOtp());
                                startActivity(new Intent(getApplicationContext(), VerificationActivity.class));
                                finish();
                            }



                    }

                    else {
                        CommonUtils.snackBar("Otp is not send on this Mobile Number.", phone_login);
                    }




                } else {
                    CommonUtils.snackBar("Otp is not send on this Mobile Number.", phone_login);
                }

            }

            @Override
            public void onFailure(retrofit2.Call<List<RegisterrationModel>> call, Throwable t) {
            }
        });
    }
    private void callLocationSateDialog(List<CountryCodeDetails> list) {
        dialog = new Dialog(context, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_occupation);
        dialog.getWindow().getAttributes().windowAnimations = animationdialog;
        ImageView leftarrow = (ImageView) dialog.findViewById(R.id.leftarrow);
        EditText editMobileNo = (EditText) dialog.findViewById(R.id.etSearch);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final ListView lv = (ListView) dialog.findViewById(R.id.listview);
        TextView saveImage = (TextView) dialog.findViewById(R.id.saveImage);
        StateAdapterDialog stateAdapterDialog = new StateAdapterDialog((Activity) context, list, this, TYPE_STATE);
        lv.setAdapter(stateAdapterDialog);
        if (state_id != null) {
            stateAdapterDialog.setSelection(state_id);
        }
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        editMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                stateAdapterDialog.getFilter().filter(arg0);
                stateAdapterDialog.getViewTypeCount();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        dialog.show();
    }

    @Override
    public void setValue(String name, String id, boolean seletion) {


        stateName = name;
        state_id = id;

        CommonUtils.savePreferencesString(context, AppConstants.COUNTRY_CODE, state_id);

        if (seletion) {


            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Thread.sleep(500);
                        dialog.dismiss();

                    } catch (InterruptedException e) {

                        e.printStackTrace();


                    }

                }
            }).start();

            tvCoutydash.setVisibility(View.VISIBLE);
            tvCountryCode.setText("+ " + state_id);
            tvCountryName.setText(stateName);

        }


    }

    @Override
    public void setValue(List<CountryCodeDetails> currentSelected, int type) {


    }
}




/*

        ArrayAdapter aa = new ArrayAdapter(context, R.layout.spinner_layout, R.id.tvspinner, Codes);
        //  spinner.getBackground().setColorFilter(getResources().getColor(R.color.navigationBarColor), PorterDuff.Mode.SRC_ATOP);
        aa.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(aa);
*/
