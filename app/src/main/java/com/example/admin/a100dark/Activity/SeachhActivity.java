package com.example.admin.a100dark.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.icu.text.IDNA;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.Adapter.SearchAdapter;
import com.example.admin.a100dark.Adapter.StateAdapterDialog;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.AllUserListModel;
import com.example.admin.a100dark.Model.ChatModel;
import com.example.admin.a100dark.Model.CountryCodeDetails;
import com.example.admin.a100dark.Model.chat_model;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.RunTimePermissionWrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

/**
 * Created by Admin on 11/3/2017.
 */

public  class SeachhActivity extends AppCompatActivity{


    RecyclerView chatrecylerview, otherrecylerview, messagerecylerview;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    SearchAdapter searchAdapter,othersearchAdapter;
    List<AllUserListDetailsModel> userList = new ArrayList<>();
    List<AllUserListDetailsModel> contactlist = new ArrayList<>();
    private EditText textEdittext;
    ImageView  cross , leftarrow;
    private Context mcontext;
    private List<CountryCodeDetails> mStringFilterList;
    private int user_id = 0;
    String[] WALK_THROUGH = new String[]{Manifest.permission.READ_CONTACTS};
    private String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS};
    private AppDatabase mAppDb;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchactivity);
        mcontext = SeachhActivity.this;
        mAppDb = MyApplication.getDb();
        CommonUtils.savePreferencesBoolean(mcontext, AppConstants.FIRST_TIME_LOGIN,true);
        user_id = CommonUtils.getIntPreferences(mcontext, AppConstants.USER_ID);
        chatrecylerview = (RecyclerView) findViewById(R.id.chatrecyler);
        otherrecylerview = (RecyclerView) findViewById(R.id.otherrecyler);
        cross = (ImageView) findViewById(R.id.cross);

        if (!hasPermissions(mcontext, PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }

        } else if (hasPermissions(mcontext, PERMISSIONS)) {
            initContactFragment();
        }


        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                textEdittext.setVisibility(View.INVISIBLE);
            }
        });
        leftarrow = (ImageView) findViewById(R.id.leftarrow);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SeachhActivity.this, MainActivity.class));
            }
        });
        textEdittext = (EditText) findViewById(R.id.textEdittext);
        searchAdapter = new SearchAdapter(userList, mcontext,userList);
        chatrecylerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        chatrecylerview.setAdapter(searchAdapter);
        callUserListApi();
        textEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                searchAdapter.getFilter().filter(arg0);
                searchAdapter.getItemViewType(arg1);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (RunTimePermissionWrapper.isAllPermissionGranted(this, PERMISSIONS))
            initContactFragment();
        else {
            showSnack(ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS[0]));
        }
    }



    private void initContactFragment() {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            AllUserListDetailsModel contactModel = new AllUserListDetailsModel();
            contactModel.setUserName(name);
            contactModel.setUserMobile(phoneNumber);
            //contactlist.add(contactModel);
           // bothlist.add(contactModel);
        }
    }


    private void callUserListApi() {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<List<AllUserListModel>> call = service.getUserListApi(user_id);
        call.enqueue(new Callback<List<AllUserListModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<AllUserListModel>> call, retrofit2.Response<List<AllUserListModel>> response) {
                List<AllUserListModel> userListModelList = response.body();

                userList.clear();

                try {

                    if (userListModelList.get(0).getCurrentUser().getUserName() != null) {
                        CommonUtils.savePreferencesString(mcontext, AppConstants.USER_NAME, userListModelList.get(0).getCurrentUser().getUserName());

                    }
                }catch (NullPointerException e) {
                    e.printStackTrace();
                }

                if (userListModelList != null && userListModelList.size() > 0) {
                    for (int i = 0; i < userListModelList.size(); i++) {
                        userList.addAll(userListModelList.get(i).getAllUser());


                    }

                    searchAdapter.notifyDataSetChanged();

                } else {
                    CommonUtils.snackBar("Not Fetched", leftarrow);
                }

                //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<List<AllUserListModel>> call, Throwable t) {
            }
        });
    }

    private void showSnack(final boolean isRationale) {
        final Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "Please provide contact permission", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(isRationale ? "VIEW" : "Settings", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();

                if (isRationale)
                    RunTimePermissionWrapper.handleRunTimePermission(SeachhActivity.this, RunTimePermissionWrapper.REQUEST_CODE.MULTIPLE_WALKTHROUGH, WALK_THROUGH);
                else
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1001);
            }
        });

        snackbar.show();
    }



}

