package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.admin.a100dark.R;

public class Help extends AppCompatActivity {
    LinearLayout arrowmessenger, linear_layoutconta_us;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        arrowmessenger = (LinearLayout)findViewById(R.id.arrowmessenger);
        linear_layoutconta_us = (LinearLayout)findViewById(R.id.linear_layoutconta_us);
        linear_layoutconta_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Help.this,Contact_us.class));


            }
        });
        arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(Help.this,MainActivity.class));


            }
        });
    }
}
