package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.admin.a100dark.R;

public class Chats extends AppCompatActivity {

    LinearLayout arrowmessenger;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);
        arrowmessenger = (LinearLayout)findViewById(R.id.arrowmessenger);
        arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Chats.this, "wait", Toast.LENGTH_SHORT).show();

           startActivity(new Intent(Chats.this,Setting.class));


            }
        });
    }
}
