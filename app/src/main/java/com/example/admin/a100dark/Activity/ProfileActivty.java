package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

public class ProfileActivty extends AppCompatActivity {
    ImageView back;
    TextView toolbarTitle;

    private TextView userNameText, editUser, currentText;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_activty);
        context = ProfileActivty.this;
        back = (ImageView) findViewById(R.id.toolbar_back);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        userNameText = (TextView) findViewById(R.id.userNameText);
        editUser = (TextView) findViewById(R.id.editUser);
        currentText = (TextView) findViewById(R.id.currentText);

        editUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivty.this, EditUsernameActivity.class));
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbarTitle.setText("Profile");

        currentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ProfileActivty.this, ChangeStatusActivity.class));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        userNameText.setText(CommonUtils.getPreferencesString(context, AppConstants.USER_NAME));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
