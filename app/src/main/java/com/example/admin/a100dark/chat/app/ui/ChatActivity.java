package com.example.admin.a100dark.chat.app.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.admin.a100dark.Activity.NewBRoasdCast;
import com.example.admin.a100dark.Activity.NewGroup;
import com.example.admin.a100dark.Activity.Setting;
import com.example.admin.a100dark.Activity.Starredmessage;
import com.example.admin.a100dark.Activity.WhatsAppWebActivity;
import com.example.admin.a100dark.Model.UploadImage;
import com.example.admin.a100dark.Model.UploadImageDetails;
import com.example.admin.a100dark.Model.UserTokenModel;
import com.example.admin.a100dark.R;

import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.db.entities.ChatMessageDto;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.chat.app.retofit.ApiInterface;
import com.example.admin.a100dark.chat.dependency.activity.DataWrapperActivity;
import com.example.admin.a100dark.chat.dependency.handler.ChstRetrofitHandler;
import com.example.admin.a100dark.chat.dependency.handler.ExecutorHandler;
import com.example.admin.a100dark.chat.dependency.handler.FCMRetrofitHandler;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.CropActivity;
import com.example.admin.a100dark.utils.GlobalAccess;
import com.example.admin.a100dark.utils.MadhuFileUploader;

import com.squareup.picasso.Picasso;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.AudioPickActivity;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.activity.VideoPickActivity;
import com.vincent.filepicker.filter.entity.AudioFile;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.vincent.filepicker.filter.entity.VideoFile;


import org.apache.http.HttpException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;


import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.example.admin.a100dark.Activity.ProfileInfo.EXCEPTION_MSG;
import static com.example.admin.a100dark.Activity.ProfileInfo.EXCEPTION_MSG_TIMEOUT;
import static com.vincent.filepicker.activity.AudioPickActivity.IS_NEED_RECORDER;
import static com.vincent.filepicker.activity.BaseActivity.IS_NEED_FOLDER_LIST;
import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;


public class ChatActivity extends DataWrapperActivity implements EasyPermissions.PermissionCallbacks {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private ChatView chatView;
    private AppDatabase mAppDb;
    private ExecutorService mExecutor;
    private String toId, myId, receiverPic = "", receiverName = "", receiverId = "",senderPic;
    private UserStatusDto userDao;
    private ImageView leftarrow, ivOption, ivProfile, ivPhone;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private ChatMessageDto msg;
    private TextView tvName;
    private String userName = "",fileName="";
    private int user_id = 0;
    private Dialog mDialog;
    public FcmMessengerViewModel fcmVM;
    private List<UserStatusDto> userList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private ApiInterface mInterface;
    private String receiver_mobile = "";
    private String chatMsg = "";
    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String mCurrentPhotoPath = "";
    private ArrayList<String> imagelist = new ArrayList<>();
    private ArrayList<UploadImageDetails> uploadList = new ArrayList<>();
    private boolean isFileImg;
    /*for Multipleimage*/
    public static final int RC_PHOTO_PICKER_PERM = 123;
    public static final int RC_FILE_PICKER_PERM = 321;
    private static final int CUSTOM_REQUEST_CODE = 532;
    private int MAX_ATTACHMENT_COUNT = 10;
    private ArrayList<String> photoPaths = new ArrayList<>();
    private ArrayList<String> docPaths = new ArrayList<>();
    private ImageView ivuserBackground, ivVedio;
    private String callerId = "";
    private String recipientId = "";
    private File file, thumbImagefile1;
    private long startTime, endTime;
    private File file3;
    private String fcmToken = "";


    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder ;
    Random random ;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer ;

    /*for audio integration data*/

    private ProgressDialog mSpinner;
    ImageView more;

    private String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NETWORK_STATE};
    private boolean videoStaus, audioStaus;
    private String type="";
    private Context mcontext;

    TextView  setting, newgroup,newbroadcast,whatsappweb,starredmessage;





    public static void start(Context activity, String myId, String myName, String toId) {
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(IApp.PARAM_2, toId);
        intent.putExtra(IApp.PARAM_3, myId);
        intent.putExtra(IApp.PARAM_4, myName);
        activity.startActivity(intent);
    }

    public static Intent getChatIntent(Context activity, String myId, String myName, String toId) {
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(IApp.PARAM_2, toId);
        intent.putExtra(IApp.PARAM_3, myId);
        intent.putExtra(IApp.PARAM_4, myName);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caht);
        mcontext=ChatActivity.this;
        CommonUtils.savePreferencesBoolean(mcontext,AppConstants.FIRST_TIME_LOGIN,true);


        user_id = CommonUtils.getIntPreferences(mContext, AppConstants.USER_ID);
        userName = CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
        mInterface = ChstRetrofitHandler.getInstance().getApi();
        mAppDb = MyApplication.getDb();
        mApi = FCMRetrofitHandler.getInstance().getApi();
        mExecutor = ExecutorHandler.getInstance().getExecutor();
        chatView = (ChatView) findViewById(R.id.chat_view);
        leftarrow = (ImageView) findViewById(R.id.leftarrow);
        tvName = (TextView) findViewById(R.id.tvName);
        ivPhone = (ImageView) findViewById(R.id.ivPhone);
        ivuserBackground = (ImageView) findViewById(R.id.ivuserBackground);
        more = (ImageView)findViewById(R.id.more);
        ivVedio = (ImageView) findViewById(R.id.ivVedio);




        if(getIntent()!=null) {

            receiverId = getIntent().getStringExtra(AppConstants.RECEIVER_USER_ID);
            toId = getIntent().getStringExtra(AppConstants.RECEIVER_USER_ID);
            receiver_mobile = getIntent().getStringExtra(AppConstants.RECEIVER_MOBILE);
            receiverPic = getIntent().getStringExtra(AppConstants.RECEIVER__PIC);
            receiverName = getIntent().getStringExtra(AppConstants.RECEIVER__USER_NAME2);

       /*    Log.e("userName", userName);
           Log.e("receiverName", receiverName);
           Log.e("RECEIVER__PIC", receiverPic);*/
        }



        if (!TextUtils.isEmpty(receiverPic)) {
            Picasso.with(mContext).load(receiverPic).into(ivuserBackground);
        }
        if (!TextUtils.isEmpty(receiverName)) {
           // tvName.setText(CommonUtils.NameCaps(receiverName+"::Luser:"+CommonUtils.getPreferencesString(mContext,AppConstants.USER_NAME)));
            tvName.setText(CommonUtils.NameCaps(receiverName));
        }
        getToken();
        setObserver();
        setListners();
        setmProgressListener(() -> {
        }, () -> {
        });
        chatView.getBinding().ivClose.setOnClickListener(v -> {
            chatView.getBinding().llSelection.setVisibility(View.GONE);
            imagelist.clear();
        });
        ivPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               audioStaus = true;
                if (!hasPermissions(mContext, PERMISSIONS)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                } else if (hasPermissions(mContext, PERMISSIONS)) {

                }
            }
        });
        ivVedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoStaus = true;
                if (!hasPermissions(mContext, PERMISSIONS)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                } else if (hasPermissions(mContext, PERMISSIONS)) {
/*
                    Intent intent = new Intent(getApplicationContext(), PopUpUserInfo.class);
                    intent.putExtra(AppConstants.SENDER_CALLER_ID, userName);
                    intent.putExtra(AppConstants.RECEIVER_CALLER_ID, receiverName);
                    intent.putExtra(AppConstants.RECEIVER_ID, toId);
                    intent.putExtra(AppConstants.DEVICE_TOKEN,fcmToken);
                    intent.putExtra(AppConstants.RECEIVER__PIC,receiverPic);
                    startActivity(intent);*/
                }


            }
        });


        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eventDialog();
            }
        });

    }

    public void eventDialog()  {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mDialog = new Dialog(mContext,android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        mDialog.getWindow().setAttributes(lp);
        View dialoglayout = inflater.inflate(R.layout.custommore, null);
        mDialog.setContentView(dialoglayout);
        setting = (TextView)mDialog.findViewById(R.id.setting);
        newgroup = (TextView)mDialog.findViewById(R.id.newgroup);
        newbroadcast= (TextView)mDialog.findViewById(R.id.newbroadcast);
        whatsappweb= (TextView)mDialog.findViewById(R.id.whatsappweb);
        starredmessage= (TextView)mDialog.findViewById(R.id.starredmeass);
        starredmessage= (TextView)mDialog.findViewById(R.id.starredmeass);
        //starredmessage= (TextView)mDialog.findViewById(R.id.starredmeass);
        mDialog.show();
        newbroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, NewBRoasdCast.class));
                mDialog.dismiss();

            }
        });
        whatsappweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, WhatsAppWebActivity.class));
                mDialog.dismiss();

            }
        });
        starredmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, Starredmessage.class));
                mDialog.dismiss();
            }
        });

        newgroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, NewGroup.class));
                mDialog.dismiss();

            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, Setting.class));
                mDialog.dismiss();
            }
        });


    }
    private void getToken() {


        CommonUtils.showProgress(mcontext);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<List<UserTokenModel>> call = service.getFcmToken(toId);
        call.enqueue(new Callback<List<UserTokenModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<UserTokenModel>> call, retrofit2.Response<List<UserTokenModel>> response) {
                CommonUtils.dismissProgress();
                List<UserTokenModel> responseBody = response.body();

                if(responseBody!=null&&responseBody.size()>0){
                    if (responseBody.get(0).getStatus()) {

                        if (responseBody.get(0).getAllUser()!=null&&responseBody.get(0).getAllUser().getDeviceToken()!=null) {

                            fcmToken = responseBody.get(0).getAllUser().getDeviceToken();
                            //  sendMsg(responseBody.get(0).getAllUser().getDeviceToken(),"");
                        }   } else {
                        Toast.makeText(mContext, "Fail Please try again not found Token", Toast.LENGTH_SHORT).show();
                    }
                }



            }

            @Override
            public void onFailure(retrofit2.Call<List<UserTokenModel>> call, Throwable t) {
            }
        });
      /*
        sdfsdg
        Observable<List<UserTokenModel>> call = mInterface.getFcmToken(toId);
        showProg();
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doFinally(() -> {
                    dismissProg();
                })
                .subscribe(responseBody -> {

                    if(responseBody!=null&&responseBody.size()>0){
                            if (responseBody.get(0).getStatus()) {

                                if (responseBody.get(0).getAllUser()!=null&&responseBody.get(0).getAllUser().getDeviceToken()!=null) {

                                    fcmToken = responseBody.get(0).getAllUser().getDeviceToken();
                                    //  sendMsg(responseBody.get(0).getAllUser().getDeviceToken(),"");
                                }   } else {
                                Toast.makeText(mContext, "Fail Please try again not found Token", Toast.LENGTH_SHORT).show();
                            }
                        }},
                        e -> {
                            Toast.makeText(mContext, "Fail Please try again Network Error", Toast.LENGTH_SHORT).show();
                        });*/

        //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
    }
    private void setListners() {
        chatView.setOnSentMessageListener(chatMessage -> {
            chatMsg = chatMessage.getMessage();

            sendMsg(chatMsg, "", ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);




            return true;
        });




        chatView.setTypingListener(new ChatView.TypingListener() {
                                       @Override
                                       public void userStartedTyping() {
                                           chatView.getActionsMenu().setIconDrawable(getResources().getDrawable(R.drawable.ic_send_white_24dp));


                                       }

                                       @Override
                                       public void userStoppedTyping() {
                                           chatView.getActionsMenu().setIconDrawable(getResources().getDrawable(R.drawable.ic_settings_voice_black_24dp));


                                       }

                                       @Override
                                       public void userClick() {

                                         startActivity(new Intent(mcontext,ActivityAudioRecording.class));




                                       }
                                   });


        chatView.setAttachmentListener(isCam -> {
            captureImage(isCam);
        });


      /*  chatView.setEmojiClickListener(v->{

            Toast.makeText(mContext, "emoji::"+, Toast.LENGTH_SHORT).show();




        });*/

        /*chatView.setEmojiClickListener(v->{

        });
        chatView.setEmojiClickListener(onEmojiClick -> {
            Toast.makeText(this, "emoji", Toast.LENGTH_SHORT).show();
        });*/


        leftarrow.setOnClickListener(view -> {
            finish();


        });




      /*  ivOption.setOnClickListener(view -> {
            ivOption.setVisibility(View.GONE);
            //  fragmentManager=((AppCompatActivity) context).getSupportFragmentManager();
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.rlContainer, new FragmentSetting());
            fragmentTransaction.commit();


        });*/

    }

    private void sendReadStatus(ChatMessageDto dto) {
        if (!dto.isRead()) {
            /*make a fcm call*/
            mExecutor.execute(() -> mAppDb.chatDao().updateData(dto.getToId(), true));
        }
    }

    private int noOfReq = 0;

    private void setObserver() {
        if (fcmVM == null) {
            fcmVM = ViewModelProviders.of(this).get(FcmMessengerViewModel.class);
            setStatusListener(fcmVM);

            /*2: getMessage*/
            fcmVM.getChatDBOns(toId + "").observe(this, chat -> {
                chatView.clearMessages();
                for (ChatMessageDto dto : chat) {
                    chatView.addMessage(new ChatMessage(dto.getMsg(), Long.parseLong(dto.getDateTime()), dto.getFilePath(), 0, dto.getMsgType()));
                }
            });

            /*4: response on getting token*/
            fcmVM.getFcmKeyObs().observe(this, res -> {
//                isTokenUpdate=true;
                if (noOfReq == 3) {
                    noOfReq = 0;
                    return;
                }
                noOfReq++;

                /*not generated here in loop*/
                userDao.setFcmKey(res.getDeviceToken());
                fcmVM.sendFcmMessage(msg, userDao.getFcmKey());
            });

            /*messenger.getMyTokenUpdateObs().observe(this, res -> {

            });*/

            /*3: if wrong token*/
            fcmVM.getCorrectTokenObs().observe(this, res -> {
                if (!res) {
                    fcmVM.getFcmToken(userDao);
                }
            });

           /* 1 : Req user*/
            fcmVM.getUserStatusObs(toId).observe(this, res -> {
                userDao = res;
            });

        }
    }

    /****************
     * Image Picking
     * ***************/
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    private static final int GALLERY_IMAGE_ACTIVITY_REQUEST_CODE = 1999;
    private static final int CAM_PER = 100;
    private static final int GAL_PER = 200;

    public void captureImage(boolean isCam) {

        if (isCam) {
            cameraPermissionMethod();

        } else {

            addThemToViewDialog();
            /*Madhu*/
            //pickPhotoClicked();

        }
    }

    private void cameraPermissionMethod() {
        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            activityForCamera();
        }

        // }
    }

    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry., There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(
                        mActivity,
                        getApplicationContext()
                                .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList =
                            getPackageManager()
                                    .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    @Override
    protected void onPermissionResult(int requestCode, boolean isPermissionGranted) {


        if (isPermissionGranted) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                activityForCamera();
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                // /*Madhu*/ activityForGallery();

            }
        }


    }

    private void cameraOperation(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            GlobalAccess.setImagePath(imageUrl);
            Intent i = new Intent(mActivity, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    String imageFile = mCurrentPhotoPath;
                    cameraOperation(imageFile);
                    break;
                //   /*Madhu*/  galleryOperation(data);
                case CROP_REQUEST_CODE:
                    if (data.getExtras().containsKey("path")) {
                        imagelist.clear();
                        photoPaths.clear();
                        mCurrentPhotoPath = data.getExtras().getString("path");
                        imagelist.add(mCurrentPhotoPath);
                        Log.e("mCurrentPhotoPath", mCurrentPhotoPath);
                        Log.e("numerofimages", imagelist.toString());
                        Log.e("sizenumerofimages", String.valueOf(imagelist.size()));
                        isFileImg = true;

                      /*  if (imagelist.size() > 0) {
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                            Picasso.with(mContext).load(mCurrentPhotoPath).into(chatView.getBinding().ivSelected);
                        }*/

                        if (imagelist.size() > 0) {
                            callProfileData();
                            imagelist.clear();
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                        } else {
                            sendMsg("Camera photos not attach", "", ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                        }


                    } else {
                        mCurrentPhotoPath = null;
                    }
                    break;


                case Constant.REQUEST_CODE_PICK_IMAGE:
                    if (resultCode == RESULT_OK) {
                        ArrayList<ImageFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                        StringBuilder builder = new StringBuilder();
                        for (ImageFile file : list) {
                            String path = file.getPath();
                            builder.append(path + "\n");
                            imagelist.add(path);
                        }
                        //   imagelist.add(builder.toString());


                        if (imagelist.size() > 0) {
                            callProfileData();
                            imagelist.clear();
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                        } else {
                            sendMsg("Gallery photos not attach", "", ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                        }


                    //    tvName.setText(builder.toString());
                    }
                    break;
                case Constant.REQUEST_CODE_PICK_VIDEO:
                    if (resultCode == RESULT_OK) {
                        ArrayList<VideoFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_VIDEO);
                        StringBuilder builder = new StringBuilder();
                        for (VideoFile file : list) {
                            String path = file.getPath();
                            String newPath="";
                            builder.append(path + "\n");
                            imagelist.add(path);
                            setCompress(path,newPath);
                          //  compressVideio(path, newPath);
                            if (imagelist.size() > 0) {
                                callProfileData();
                                imagelist.clear();
                                chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                            } else {
                                sendMsg("Video not attach", "", ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                            }


                        }

                    }
                    break;
                case Constant.REQUEST_CODE_PICK_AUDIO:
                    if (resultCode == RESULT_OK) {
                        ArrayList<AudioFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_AUDIO);
                        StringBuilder builder = new StringBuilder();
                        for (AudioFile file : list) {
                            String path = file.getPath();
                            builder.append(path + "\n");
                            imagelist.add(path);
                        }

                     //   tvName.setText(builder.toString());


                        if (imagelist.size() > 0) {
                            callProfileData();
                            imagelist.clear();
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                        } else {
                            sendMsg("audio not attach", "", ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                        }
                    }
                    break;
                case Constant.REQUEST_CODE_PICK_FILE:
                    if (resultCode == RESULT_OK) {
                        ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                        StringBuilder builder = new StringBuilder();
                        for (NormalFile file : list) {
                            String path = file.getPath();
                            builder.append(path + "\n");
                            imagelist.add(path);
                            fileName=file.getName();


                        }



                       // tvName.setText(builder.toString());
                        if (imagelist.size() > 0) {
                            callProfileData();
                            imagelist.clear();
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                        } else {
                            sendMsg("File not attach" +
                                    "", "", ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                        }
                    }
                    break;
            }

        }

        //  addThemToView(photoPaths, docPaths);
        // addThemToViewDialog();
        //  imagelist.addAll(photoPaths);


        if (imagelist.size() > 0) {
            chatView.getBinding().llSelection.setVisibility(View.GONE);
            Picasso.with(mContext).load(imagelist.get(imagelist.size() - 1)).into(chatView.getBinding().ivSelected);
        }


    }


    @SuppressWarnings("deprecation")
    public static Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }


    private Locale getLocale() {
        Configuration config = getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }

        return sysLocale;
    }

    private void setCompress(String path, String newPath1) {

        try {

            file = new File(new URI("file://" + path.replace(" ", "%20")));
            long fileSizeInBytes = file.length();

            Toast.makeText(this, "withoutCompress::"+path.length(), Toast.LENGTH_SHORT).show();
            String newPath = "/storage/emulated/0/" + new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";
            //  String desPath = "StemBuddy" + File.separator + "VIDEO_" + new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";

            long fileSizeInKB = fileSizeInBytes / 1024;
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            long fileSizeInMB = fileSizeInKB / 1024;
            Bitmap tmb = ThumbnailUtils.createVideoThumbnail(path,
                    MediaStore.Images.Thumbnails.MINI_KIND);
            String image_path = getRealPathFromURI(getImageUri(mContext, tmb));
            thumbImagefile1 = new File(new URI("file://" + image_path.replace(" ", "%20")));

            Toast.makeText(this, "file1Thumbnail::"+ thumbImagefile1.length(), Toast.LENGTH_SHORT).show();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void sendMsg(String chatMsg, String filePath, int msgType) {
        ChatMessageDto msg = new ChatMessageDto(
                System.currentTimeMillis() + "",
                String.valueOf(user_id) + "",
                CommonUtils.getPreferences(mContext, AppConstants.USER_NAME) + "",
                "https://www.pexels.com/search/scenery/",
                toId + "",
                receiverName + "",
                fcmToken + "",
                false,
                false,
                true,
                msgType,
                chatMsg,
                filePath);


        fcmVM.sendFcmMessage(msg, userDao.getFcmKey());
    }

    private void callProfileData() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", String.valueOf(user_id));
        params.put("thumb_image", String.valueOf(thumbImagefile1));
        MadhuFileUploader uploader = new MadhuFileUploader();
        uploader.uploadFile(params, imagelist, "fileToUpload", mContext);

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<List<UploadImage>> call = service.uploadImageList(uploader.getPartMap(), uploader.getPartBodyArr());
        // Call<List<UploadImage>> call = service.uploadFileFromLib(uploader.getPartMap(),uploader.getPartBody());
        call.enqueue(new Callback<List<UploadImage>>() {
            public boolean status;

            @Override
            public void onResponse(Call<List<UploadImage>> call, final retrofit2.Response<List<UploadImage>> response) {

                List<UploadImage> uploadImagesList = new ArrayList<>();
                uploadImagesList = response.body();
                uploadList.clear();

                if (uploadImagesList.get(0).getStatus()) {
                    uploadList.addAll(uploadImagesList.get(0).getAllImage());
                    type = uploadImagesList.get(0).getAllImage().get(0).getExtn();

                    if(uploadImagesList.get(0).getAllImage().get(0).getType()!=null&&uploadImagesList.get(0).getAllImage().size()>0){
                        if(uploadImagesList.get(0).getAllImage().get(0).getType()!=null){
                         //   type = uploadImagesList.get(0).getAllImage().get(0).getType().get(1);
                        }

                        Log.e("typeAllImages",type);
                    }
                   /*
                     if(uploadImagesList.get(0).getAllImage().get(0).getType().get(0)!=null){
                        type = uploadImagesList.get(0).getAllImage().get(0).getType().get(0);
                    }

                   switch (type) {
                        case "image":
                            Toast.makeText(ChatActivity.this, "image", Toast.LENGTH_SHORT).show();
                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3);
                            }
                            break;
                        case "audio":
                            Toast.makeText(ChatActivity.this, "audio", Toast.LENGTH_SHORT).show();

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7);
                            }
                            Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;

                        case "video":
                            Toast.makeText(ChatActivity.this, "video", Toast.LENGTH_SHORT).show();

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_VIDEO_5);
                            }
                            Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;

                    }*/

                    switch (type) {
                        case "jpeg":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3);
                            }
                           // Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();

                            break;

                        case "jpg":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3);
                            }
                           // Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;

                        case "png":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3);
                            }
                          //  Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;

                        case "mp3":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7);
                            }
                           // Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;

                        case "mpeg":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7);
                            }
                          // Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;
                        case "mp4":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_VIDEO_5);
                            }
                            //Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;

                        case "pdf":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_DOC_9);
                            }
                          //  Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;

                        case "doc":

                            for (UploadImageDetails upload : uploadImagesList.get(0).getAllImage()) {
                                sendMsg(chatMsg, upload.getImage(), ChatMessage.Type.TYPE_VIEW_SEN_MSG_DOC_9);
                            }
                            Toast.makeText(ChatActivity.this, type, Toast.LENGTH_SHORT).show();
                            break;
                    }


                }

            }

            @Override
            public void onFailure(Call<List<UploadImage>> call, Throwable t) {

                //   dialog.dismiss();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = EXCEPTION_MSG_TIMEOUT;
                } else if (t instanceof IOException) {
                    msg = EXCEPTION_MSG_TIMEOUT;

                } else if (t instanceof HttpException) {
                    msg = EXCEPTION_MSG;
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }

    private void showProg() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    private void dismissProg() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }


    private void addThemToViewDialog() {
        TextView tvPhotos, tvVideo, tvAudio, tvFile;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.85f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        mDialog.getWindow().setAttributes(lp);
        View dialoglayout = inflater.inflate(R.layout.gallery_popup_profile_pic, null);
        mDialog.setContentView(dialoglayout);

        tvPhotos = (TextView) mDialog.findViewById(R.id.tvPhotos);
        tvVideo = (TextView) mDialog.findViewById(R.id.tvVideo);
        tvAudio = (TextView) mDialog.findViewById(R.id.tvAudio);
        tvFile = (TextView) mDialog.findViewById(R.id.tvFile);
        mDialog.show();

     /*   tvFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imagelist.size() > 0) {
                    callProfileData();
                    imagelist.clear();
                    chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                } else {
                    sendMsg(tvFile.getText().toString(), "", ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                }
                mDialog.dismiss();
            }
        });*/

        tvPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent1 = new Intent(mContext, ImagePickActivity.class);
                intent1.putExtra(IS_NEED_CAMERA, true);
                intent1.putExtra(Constant.MAX_NUMBER, 9);
                intent1.putExtra(IS_NEED_FOLDER_LIST, true);
                startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_IMAGE);

                mDialog.dismiss();
            }
        });
        tvVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent2 = new Intent(mContext, VideoPickActivity.class);
                intent2.putExtra(IS_NEED_CAMERA, true);
                intent2.putExtra(Constant.MAX_NUMBER, 9);
                intent2.putExtra(IS_NEED_FOLDER_LIST, true);
                startActivityForResult(intent2, Constant.REQUEST_CODE_PICK_VIDEO);

                mDialog.dismiss();
            }
        });
        tvAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent3 = new Intent(mContext, AudioPickActivity.class);
                intent3.putExtra(IS_NEED_RECORDER, true);
                intent3.putExtra(Constant.MAX_NUMBER, 9);
                intent3.putExtra(IS_NEED_FOLDER_LIST, true);
                startActivityForResult(intent3, Constant.REQUEST_CODE_PICK_AUDIO);



                mDialog.dismiss();
            }
        });
        tvFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent4 = new Intent(mContext, NormalFilePickActivity.class);
                intent4.putExtra(Constant.MAX_NUMBER, 9);
                intent4.putExtra(IS_NEED_FOLDER_LIST, true);
                intent4.putExtra(NormalFilePickActivity.SUFFIX,
                        new String[]{"xlsx", "xls", "doc", "dOcX", "ppt", ".pptx", "pdf"});
                startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);

                mDialog.dismiss();
            }
        });
    }





//For Video Integration

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




    @Override
    protected void onPause() {
        if (mSpinner != null) {
            mSpinner.dismiss();
        }
        super.onPause();
    }






    private void showSpinner() {
        mSpinner = new ProgressDialog(this);
        mSpinner.setTitle("Logging in");
        mSpinner.setMessage("Please wait...");
        mSpinner.show();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }

//TODO for AudioChat






}


