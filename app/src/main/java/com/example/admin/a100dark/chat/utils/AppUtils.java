package com.example.admin.a100dark.chat.utils;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.media.RingtoneManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.MainActivity;
import com.example.admin.a100dark.Activity.PopUpUserInfo;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.db.entities.ChatMessageDto;
import com.example.admin.a100dark.chat.app.ui.ChatActivity;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.google.gson.Gson;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import co.intentservice.chatui.models.ChatMessage;


/**
 * Created by abul on 28/10/17.
 */

public class AppUtils implements IConstants.IFcm {

    public static String NOTIF_MSG = "HideAll has new msg";
    public static final String NOTIF_MSG_HEADEING = "HideAll";

    /*****************************
     * Notification
     * ****************************/
    public static void showNotification2(Context context, NotificationDto dto, int count) {

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        android.app.Notification not=buildNotification(context, dto, count).build();
        if(not==null){
            return;
        }
        notificationManager.notify(count,not);
    }


    /*************
     * custom
     * ***************/
    protected static NotificationCompat.Builder buildNotification(Context context, NotificationDto dto, int count) {


        // Open NotificationView.java Activity
        Intent intent =null;

        switch (dto.getType()) {
            case ChatMessage.Type.TYPE_VIEW_REC_MSG_TEXT_0:
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1:
                ChatMessageDto chat=new Gson().fromJson(dto.getMsg(),ChatMessageDto.class);
                NOTIF_MSG=chat.getMsg();
                intent = ChatActivity.getChatIntent(context,chat.getToId(), CommonUtils.getPreferences(context, AppConstants.USER_NAME),chat.getMyId());
                break;

            case FCM_STATUS_MSG:

                break;
            case FCM_NEW_REQUEST:
                NOTIF_MSG=dto.getMsg();
                intent = MainActivity.getPendingIntent(context, IConstants.IFcm.FCM_RECIEVER);
                break;
            case FCM_APP_REQUEST:
                NOTIF_MSG=dto.getMsg();
                intent = MainActivity.getPendingIntent(context, IConstants.IFcm.FCM_SENDER);
                break;



        }

        /*if(intent==null){
            return null;
        }*/

//        intent.putExtra(IConstants.IApp.PARAM_1, dto.getMsg());
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent pIntent = PendingIntent.getActivity(
                context,
                count,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        /*Uri customSound = Uri.parse(context.getContentResolver().SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/raw/coins");*/

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(NOTIF_MSG_HEADEING)
                .setContentText(NOTIF_MSG)
                // Set Ticker MessageData
                .setTicker(NOTIF_MSG_HEADEING)
                // Dismiss Notification
                .setAutoCancel(true)
                .setVibrate(new long[]{500, 100, 600})
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(bmp);
        if(intent!=null){
            // Set PendingIntent into Notification
                builder=builder.setContentIntent(pIntent);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // build a complex notification, with buttons and such
            //
            builder = builder.setCustomBigContentView(getComplexNotificationView(context, NOTIF_MSG));
        } else {
            // Build a simpler notification, without buttons
            builder = builder.setContentTitle(NOTIF_MSG_HEADEING)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(NOTIF_MSG))
//                    .setContentText(msg)
                    .setSmallIcon(android.R.drawable.ic_menu_gallery);
        }
        return builder;
    }

    /***********************
     * Remote view
     * **************************/
    private static RemoteViews getComplexNotificationView(Context context, String msg) {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews notificationView = new RemoteViews(
                context.getPackageName(),
                R.layout.notification_layout
        );

        // Locate and set the Image into customnotificationtext.xml ImageViews
        notificationView.setImageViewResource(
                R.id.imagenotileft,
                R.mipmap.ic_launcher);

        // Locate and set the Text into customnotificationtext.xml TextViews
        notificationView.setTextViewText(R.id.title, NOTIF_MSG_HEADEING);
        notificationView.setTextViewText(R.id.text, msg);

        return notificationView;
    }


    public static void saveImage(Context context, Bitmap bitmap, String name) {
        File outputDir = new File(Environment.getExternalStorageDirectory() + "/abul"); // context being the Activity pointer
        File tempFile = null;
        try {
            tempFile = File.createTempFile(name, ".jpg", outputDir);
            FileOutputStream outputStream = new FileOutputStream(tempFile);
            boolean compress = bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
            Toast.makeText(context, "no error", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }


    public static boolean isAppRunning(final Context context) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(context.getApplicationContext().getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }


    public static String isRunning(Context ctx) {
        ActivityManager mActivityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);

        return mActivityManager.getRunningTasks(1).get(0).topActivity.getClassName();

    }


}
