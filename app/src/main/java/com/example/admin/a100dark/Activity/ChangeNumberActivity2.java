package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.AccountActivity;
import com.example.admin.a100dark.Activity.Chats;
import com.example.admin.a100dark.Activity.Contacts;
import com.example.admin.a100dark.Activity.DataUsage;
import com.example.admin.a100dark.Activity.Help;
import com.example.admin.a100dark.Activity.Notitication;
import com.example.admin.a100dark.Activity.ProfileActivty;

import com.example.admin.a100dark.Activity.Setting;
import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class ChangeNumberActivity2 extends AppCompatActivity {


    LinearLayout rightDoneLinear;

    EditText old_code_edit, new_code_edit, old_number_edit, new_number_edit;

    String oldCountryCode, oldMobileNo;
    private Context context;
    int userId;

    FileUploadInterface fileUploadInterface;

    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number2);

        rightDoneLinear = findViewById(R.id.rightDoneLinear);
        old_code_edit = findViewById(R.id.old_country_edit);
        new_code_edit = findViewById(R.id.new_country_edit);
        old_number_edit = findViewById(R.id.old_phone_edit);
        new_number_edit = findViewById(R.id.new_phone_edit);

        fileUploadInterface = RetrofitHandler.getInstance().getApi();

        oldCountryCode = CommonUtils.getPreferencesString(context, AppConstants.COUNTRY_CODE);
        oldMobileNo = CommonUtils.getPreferencesString(context, AppConstants.Registerd_MOBILE_NO);
        userId = CommonUtils.getIntPreferences(context, AppConstants.USER_ID);

        rightDoneLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String oldumber = old_number_edit.getText().toString();
                String newumber = new_number_edit.getText().toString();
                String oldCode = old_code_edit.getText().toString();
                String newCode = new_code_edit.getText().toString();
                if (old_code_edit.length() == 0 || new_code_edit.length() == 0) {
                    showAlertDialog("Please enter phone number");
                } else if (oldumber.equals(newumber)) {
                    showAlertDialog("Old number and new number are same");
                } else {
                    submitNumber(oldumber, newumber, oldCode, newCode);
                }
            }
        });
    }

    private void submitNumber(String oldumber, String newumber, String oldCode, String newCode) {

        Call<ResponseBody> bodyCall = fileUploadInterface.updateUserNo(oldumber, newumber, oldCode, newCode, String.valueOf(userId));
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        int status = jsonObject.getInt("status");
                        String msg = jsonObject.getString("msg");

                        if (status == 1) {
                            String OTP = jsonObject.getString("OTP");
                            showOTPDialog(OTP);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void showOTPDialog(String otp) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChangeNumberActivity2.this);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.otp_dialog, null);
        builder.setCancelable(false);
        builder.setView(view);

        EditText otpEdit = view.findViewById(R.id.otp_edit);
        Button otpbutton = view.findViewById(R.id.otp_button);

        otpbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpEdit.length() == 0) {
                    Toast.makeText(context, "Please enter OTP", Toast.LENGTH_SHORT).show();
                } else if (!otpEdit.getText().toString().equals(otp)) {
                    Toast.makeText(context, "Please enter correct OTP", Toast.LENGTH_SHORT).show();
                } else {
                    builder.create().dismiss();
                }
            }
        });

        builder.show();
    }

    private void showAlertDialog(String s) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChangeNumberActivity2.this);
        builder.setMessage(s);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        builder.show();
    }
}