package com.example.admin.a100dark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 11/1/2017.
 */

public class UserTokenModel {


        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("allUser")
        @Expose
        private UserTokenDetailsModel allUser;

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public UserTokenDetailsModel getAllUser() {
            return allUser;
        }

        public void setAllUser(UserTokenDetailsModel allUser) {
            this.allUser = allUser;
        }







}
