package com.example.admin.a100dark.Fragment;

import android.app.Activity;
import android.arch.lifecycle.GeneratedAdapter;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.SeachhActivity;
import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.Adapter.NewUserChatAdapter;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.AllUserListModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.chat.dependency.activity.SuperActivity;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

public class NewUserFragment extends Fragment {

    private TextView tvContactDetails,tvContactDetails2;
    private FloatingActionButton fabChat;
    private RecyclerView rvUserList;
    private NewUserChatAdapter chatAdapter;
    private List<AllUserListDetailsModel> userList = new ArrayList<>();
    private Context mcontext;
    private Activity activity;
    private int user_id=0;




    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View vv = inflater.inflate(R.layout.new_user, null);
        mcontext=getActivity();
        user_id=CommonUtils.getIntPreferences(mcontext, AppConstants.USER_ID);


        fabChat=vv.findViewById(R.id.fabNewUser1);
        rvUserList=vv.findViewById(R.id.rvUserList);
        tvContactDetails2=vv.findViewById(R.id.tvContactDetails2);
        tvContactDetails=vv.findViewById(R.id.tvContactDetails);
        chatAdapter = new NewUserChatAdapter(userList,mcontext);
        rvUserList.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL,true));
        rvUserList.setAdapter(chatAdapter);


        callUserListApi();

        fabChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(mcontext, SeachhActivity.class));

                /*if(listner!=null){
                    listner.navigateToChat();

                }*/

            }
        });




        return vv;
    }




    private void callUserListApi() {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<List<AllUserListModel>> call = service.getUserListApi(user_id);
        call.enqueue(new Callback<List<AllUserListModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<AllUserListModel>> call, retrofit2.Response<List<AllUserListModel>> response) {
                List<AllUserListModel> userListModelList = response.body();

                userList.clear();
                if (userListModelList.get(0).getCurrentUser()!=null&&userListModelList.get(0).getCurrentUser().getUserName() != null) {

                    CommonUtils.savePreferencesString(mcontext, AppConstants.USER_NAME, userListModelList.get(0).getCurrentUser().getUserName());


                    List<UserStatusDto> users = new ArrayList<>();

                    if (userListModelList != null && userListModelList.size() > 0) {
                        for (int i = 0; i < userListModelList.size(); i++) {
                            userList.addAll(userListModelList.get(i).getAllUser());

                        }
                            if(userList.size()==1) {
                                tvContactDetails.setText(CommonUtils.NameCaps(userList.get(0).getUserName()));
                            }


                       else  if(userList.size()==2) {
                                tvContactDetails.setText(CommonUtils.NameCaps(userList.get(0).getUserName() + ", " +
                                        CommonUtils.NameCaps(userList.get(1).getUserName())));

                            }


                         else    if(userList.size()==3) {
                                tvContactDetails.setText(CommonUtils.NameCaps(userList.get(0).getUserName() + ", " +
                                        CommonUtils.NameCaps(userList.get(1).getUserName()) + ", " + CommonUtils.NameCaps(userList.get(2).getUserName())));

                            }


                         else    if(userList.size()==4) {
                                tvContactDetails.setText(CommonUtils.NameCaps(userList.get(0).getUserName() + ", " +
                                        CommonUtils.NameCaps(userList.get(1).getUserName()) + ", " + CommonUtils.NameCaps(userList.get(2).getUserName()) + ", "
                                        + CommonUtils.NameCaps(userList.get(3).getUserName())));

                            }


                       else      if(userList.size()==5) {
                                tvContactDetails.setText(CommonUtils.NameCaps(userList.get(0).getUserName() + ", " +
                                        CommonUtils.NameCaps(userList.get(1).getUserName()) + ", " + CommonUtils.NameCaps(userList.get(2).getUserName()) + ", "
                                        + CommonUtils.NameCaps(userList.get(3).getUserName()) + ", " + CommonUtils.NameCaps(userList.get(4).getUserName())));

                                tvContactDetails2.setText(String.valueOf(userList.size() - 5) + " more of your contacts are on HideAll");
                            }

                            else  {
                                tvContactDetails.setText(CommonUtils.NameCaps(userList.get(0).getUserName() + ", " +
                                        CommonUtils.NameCaps(userList.get(1).getUserName()) + ", " + CommonUtils.NameCaps(userList.get(2).getUserName()) + ", "
                                        + CommonUtils.NameCaps(userList.get(3).getUserName()) + ", " + CommonUtils.NameCaps(userList.get(4).getUserName())));

                                tvContactDetails2.setText(String.valueOf(userList.size() - 5) + " more of your contacts are on HideAll");
                            }
                        chatAdapter.notifyDataSetChanged();
                        }


                    } else {
                        CommonUtils.snackBar("Not Fetched", rvUserList);
                    }

                    //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
                }


            @Override
            public void onFailure(retrofit2.Call<List<AllUserListModel>> call, Throwable t) {
            }
        });
    }

    ContainerChatFragment.INavigation listner;

    public void setListener(ContainerChatFragment.INavigation listner){
        this.listner=listner;
    }

}
